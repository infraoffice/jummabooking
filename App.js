/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import store from './src/redux_store/store';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';


import SplashScreen from './src/screen/SplashScreen';
import Colors from './src/common/Colors';

import { run_database_migrations } from './src/database/AppDatabase';
import LoginScreen from './src/screen/LoginScreen';
import SignUpScreen from './src/screen/SignUpScreen';
import HomeScreen from './src/screen/HomeScreen';
import SuggetionScreen from './src/screen/SuggetionScreen';
import SelectLanguageScreen from './src/screen/SelectLanguageScreen';
import OTPVerificationScreen from './src/screen/OTPVerificationScreen';
import MobileVerificationScreen from './src/screen/MobileVerificationScreen';
import NotificationAlertScreen from './src/screen/NotificationAlertScreen';
import MyBookingScreen from './src/screen/MyBookingScreen';
import ChangeLanguage from './src/screen/ChangeLanguage';
import ProfileRegistrationScreen from './src/screen/ProfileRegistrationScreen';
import BookingConfirmationScreen from './src/screen/BookingConfirmationScreen';
import MasjidInformationScreen from './src/screen/MasjidInformationScreen';




// console.disableYellowBox = true;
const Stack = createStackNavigator();
class App extends Component {
  constructor(props) {
    super(props);
    run_database_migrations().then(() => {
      // console.log("creating table");
    });
  }

  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.themeColor}
            barStyle={"light-content"}
          />
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{ animationEnabled: false, headerShown: false }}
            >

              <Stack.Screen name="SplashScreen" component={SplashScreen} />
              <Stack.Screen name="LoginScreen" component={LoginScreen} />
              <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
              <Stack.Screen name="HomeScreen" component={HomeScreen} />
              <Stack.Screen name="SuggetionScreen" component={SuggetionScreen} />
              <Stack.Screen name='SelectLanguageScreen' component={SelectLanguageScreen} />
              <Stack.Screen name='MobileVerificationScreen' component={MobileVerificationScreen} />
              <Stack.Screen name='OTPVerificationScreen' component={OTPVerificationScreen} />
              <Stack.Screen name='NotificationAlertScreen' component={NotificationAlertScreen} />
              <Stack.Screen name='MyBookingScreen' component={MyBookingScreen} />
              <Stack.Screen name='ChangeLanguage' component={ChangeLanguage} />
              <Stack.Screen name='ProfileRegistrationScreen' component={ProfileRegistrationScreen} />
              <Stack.Screen name='BookingConfirmationScreen' component={BookingConfirmationScreen} />
              <Stack.Screen name='MasjidInformationScreen' component={MasjidInformationScreen} />
              
            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default App;
