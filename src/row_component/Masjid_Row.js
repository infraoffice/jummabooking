import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';
import Colors from '../common/Colors';
import { debugLog } from '../common/Constants';
import { getWidth } from '../common/Layout';
import ProgressCircle from 'react-native-progress-circle'
import Font from '../common/Font';

const Masjid_Row = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={[styles.container, props.style]}>

            {
                props.data_row.totalBooking < 100 ?
                    <View>
                        <Text style={{ fontSize: 16, color: Colors.headingColor, fontWeight: "500", fontFamily: Font.Roboto }}>{props.data_row.name} </Text>
                        <Text style={{ fontSize: 12, color: Colors.textColor, fontWeight: "400", paddingVertical: 4, }}>{props.data_row.address} </Text>
                        <Text style={{ fontSize: 12, color: Colors.headingColor, fontWeight: "400", paddingTop: 5, }}>{props.data_row.time} </Text>
                    </View>
                    :
                    <View>
                        <Text style={{ fontSize: 16, color: Colors.textColor, fontWeight: "500", fontFamily: Font.Roboto }}>{props.data_row.name} </Text>
                        <Text style={{ fontSize: 12, color: Colors.textColor, fontWeight: "400", paddingVertical: 4, }}>{props.data_row.address} </Text>
                        <Text style={{ fontSize: 12, color: Colors.textColor, fontWeight: "400", paddingTop: 5, }}>{props.data_row.time} </Text>
                    </View>
            }


            <View style={{ alignItems: 'center' }} >

                {
                    props.data_row.totalBooking < 100 ?
                        <ProgressCircle
                            percent={props.data_row.totalBooking}
                            radius={20}
                            borderWidth={3}
                            color={Colors.redColor}
                            shadowColor={Colors.greenColor}
                            bgColor="#fff"
                        >
                            <Text style={styles.txtStyle}>{props.data_row.totalBooking + '%'}</Text>
                        </ProgressCircle>
                        :
                        null
                }

                {
                    props.data_row.totalBooking < 100 ?

                        <TouchableOpacity
                            onPress={() => props.onBookNow(props.data_row)}
                            style={{
                                borderRadius: 30,
                                backgroundColor: Colors.whiteColor,
                                elevation: 1,
                                width: 73,
                                height: 23,
                                marginTop: 10,
                                borderWidth: 1,
                                borderColor: Colors.greenColor,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <Text style={{
                                color: Colors.greenColor, fontSize: 10, textAlign: 'center',
                                paddingHorizontal: 10,
                                paddingVertical: 4,
                                // maxWidth: 73,
                            }} >
                                {props.book_now}
                            </Text>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity
                            onPress={props.onBookFull}
                            style={{
                                borderRadius: 30,
                                backgroundColor: Colors.whiteColor,
                                elevation: 1,
                                // width: 73,
                                // height: 23,
                                marginTop: 10,
                                borderWidth: 1,
                                borderColor: Colors.redColor,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <Text style={{
                                color: Colors.redColor,
                                fontSize: 10,
                                //  width: 73,
                                // height: 23,
                                textAlign: 'center',
                                paddingHorizontal: 10,
                                paddingVertical: 4,
                                minWidth: 73,
                            }} >
                                {props.full_booked}
                            </Text>
                        </TouchableOpacity>
                }


            </View>

        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        height: 110,
        backgroundColor: Colors.whiteText,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: Colors.borderColor,
        marginRight: 20,
        marginLeft: 30
    },
    imgStyle: {
        height: getWidth(40),
        width: getWidth(40)
    },
    txtStyle: {
        fontSize: 10,
        color: Colors.headingColor,
        fontStyle: 'normal',
        fontWeight: '500',
        fontFamily: Font.Roboto
    },

});
export default Masjid_Row;
