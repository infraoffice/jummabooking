import { run_migration_v1 } from './migration_v1';
import { executeSql } from './DatabaseExecutor';
import { debugLog } from '../common/Constants';

export const run_database_migrations = async () => {
  console.log('running database migration');
  await run_migration_v1();
};




export const insertServiceList = async (serviceArray) => {
  const table_name = 'service_types';
  let query = `insert into ${table_name}`;
  query += `(service_name)`;
  query += `VALUES (?)`;
  // debugLog(serviceArray)

  try {
    serviceArray.map(async (item) => {
      let result = await executeSql(`select * from ${table_name} where service_name = '${item.item_name}'`, []);
      debugLog("array list>>>")
      let array = result.rows;
      if (array.length === 0) {
        await executeSql(query, [item.item_name.toUpperCase()]);
        console.log('record inserted...');
      }
    });
    selectAllRecord(`SELECT * FROM ${table_name}`);
    console.log(`Record inserted successfully ${table_name} `);
  } catch (error) {
    console.log('Record is not inserted!');
  }
};

// export const insertAppLanguageContent = async (AR,EN,UR,BN) => {
//   const table_name = 'app_language_content';
//   let query = `insert into ${table_name}`;
//   query += `(AR,EN,UR,BN)`;
//   query += `VALUES (?,?,?,?)`;

//   try {
//       let result = await executeSql(`select * from ${table_name} where EN = '${EN}'`, []);
//       let array = result.rows;
//       if (array.length === 0) {
//         await executeSql(query, [AR,EN,UR,BN]);
//         console.log('record inserted...');
//       }

//     selectAllRecord(table_name);
//     console.log(`Record inserted successfully ${table_name} `);
//   } catch (error) {
//     console.log('Record is not inserted!');
//   }
// };

export const insertAppLanguageContent = async () => {
  const table_name = 'app_language_content';
  let query = `insert into ${table_name}`;
  query += ` (AR,EN,UR,BN) VALUES `;

  query += `('تسجيل الدخول', 'Sign in','سائن ان', 'সাইন ইন করুন'),`;
  query += `('تسجيل الدخول', 'Login','لاگ ان کریں', 'প্রবেশ করুন'),`;
  query += `('رقم الهاتف المحمول', 'Mobile Number','موبائل نمبر', 'সাইন ইন করুন'),`;
  query += `('أدخل هاتفك المحمول هنا', 'Enter your mobile here','اپنا موبائل یہاں داخل کریں', 'আপনার মোবাইল এখানে প্রবেশ করুন'),`;
  query += `('كلمه السر', 'Password','پاس ورڈ', 'পাসওয়ার্ড'),`;
  query += `('نسيت كلمة المرور؟', 'Forget Password?','پاس ورڈ بھول جانا؟', 'পাসওয়ার্ড ভুলে গেছেন?'),`;
  query += `('أو', 'OR','یا', 'বা'),`;
  query += `('انشئ حساب', 'Create an account','کھاتا کھولیں', 'একটি অ্যাকাউন্ট তৈরি করুন')`;
debugLog(query);
  // Sign up
  // query += `('انشئ حساب', 'Back','کھاتا کھولیں', 'পেছনে')`;



  try {
      // let result = await executeSql(`select * from ${table_name} where EN = '${EN}'`, []);
      // debugLog("array list>>>")
      // let array = result.rows;
      // if (array.length === 0) {
        await executeSql(query, []);
        console.log('batch record inserted...');

      // }
  //  selectAllRecord(table_name);
  getLangText("Login",'AR');
    console.log(`Record inserted successfully ${table_name} `);
  } catch (error) {
    console.log('Record is not inserted!');
  }
};
export const getLangText = async (key,language) => {
  let table_name='app_language_content';
  let selectQuery = await executeSql(`SELECT ${language} FROM ${table_name} where EN='${key}'`, []);
  var rows = selectQuery.rows;
  var array = [];
  // for (let i = 0; i < rows.length; i++) {
    array[0] = rows.item(0);
    console.log(array[0][language]);
  // }
  return array[0][language];
};

// Select record
export const selectAllRecord = async (table_name) => {
  let selectQuery = await executeSql(`SELECT * FROM ${table_name}`, []);
  var rows = selectQuery.rows;
  var array = [];
  for (let i = 0; i < rows.length; i++) {
    array[i] = rows.item(i);
    console.log(array[i]);
  }
  return array;
};

// Update Record
export const updateService = async (id, name) => {
  const table_name = 'users_name';
  let query = `update ${table_name} set name = '${name}' where id = ${id} `;

  try {
    await executeSql(query, []);
    console.log('user updated');
  } catch (error) {
    console.log(error);
  }
};

// Delete record
export const deleteLanguageContent = async () => {
  const table_name = 'app_language_content';
  // let query = `delete from ${table_name} where id = ${id}`;
  // let query = `delete from ${table_name}`;
  let query = `DROP TABLE ${table_name}`;


  try {
    await executeSql(query, []);
    console.log('deleted!');
  } catch (error) {
    console.log(error);
  }
};