import { executeSql } from './DatabaseExecutor';

const migration_name = `create_base_tables`;

export const run_migration_v1 = async () => {
  await createAppLanguageContent();
};


const createAppLanguageContent = async (db) => {
  let table_name = 'app_language_content';
  let query = `create table if not exists ${table_name} (`;
  query += `language_id integer primary key AUTOINCREMENT,`;
  query += `AR text,`;
  query += `EN text, `;
  query += `UR text, `;
  query += `BN text )`;
  try {
    let results = await executeSql(query, []);
    console.log(`created ${table_name} success!`);
  } catch (error) {
    console.log(`creation error on ${table_name}` + JSON.stringify(error));
  }
};
