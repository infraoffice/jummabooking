import { debugLog } from '../../common/Constants';
import store from '../store';
import {
   SET_CURRENT_LANG, SET_DROPDOWN_PLACEHOLDER, SET_IS_DROPDOWN,
} from './types';


export const setCurrentLang = (title,value) => (dispatch) => {
  debugLog(value)
  dispatch({
    type: SET_CURRENT_LANG,
    payload: value,
  });

  dispatch({
    type: SET_DROPDOWN_PLACEHOLDER,
    payload: title,
  });

};

export const setDropdownIsTrue = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_IS_DROPDOWN,
    payload: isTrue,
  });
};