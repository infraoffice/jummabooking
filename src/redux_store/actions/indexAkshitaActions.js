import store from '../store';

import { IS_ALREADY_BOOKED, IS_CHECKED, OTP_FIRST, OTP_FOURTH, OTP_SECOND, OTP_THIRD, SET_LOGIN_MOBILE, SET_LOGIN_PASS, SET_OTP, SET_PROFILE_PIC, SET_SIGNUP_ADDRESS_AREA, SET_SIGNUP_ADDRESS_BLOCK, SET_SIGNUP_ADDRESS_BUILDING, SET_SIGNUP_ADDRESS_ROAD, SET_SIGNUP_CPR_NO, SET_SIGNUP_MOBILE, SET_SIGNUP_NAME, SET_SIGNUP_PASS, SET_SIGNUP_RE_PASS } from './typesAkshita';

export const setLoginMobile = (login_mobile) => (dispatch) => {
  console.log(login_mobile);
  dispatch({
    type: SET_LOGIN_MOBILE,
    payload: login_mobile,
  });
};

export const setLoginPass = (login_pass) => (dispatch) => {
  dispatch({
    type: SET_LOGIN_PASS,
    payload: login_pass,
  });
};


// sign up action
export const setSignupName = (signup_name) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_NAME,
    payload: signup_name,
  });
};
export const setSignupCprNo = (signup_cpr_no) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_CPR_NO,
    payload: signup_cpr_no,
  });
};
export const setSignupMobile = (signup_mobile) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_MOBILE,
    payload: signup_mobile,
  });
};
export const setSignupPass = (signup_pass) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_PASS,
    payload: signup_pass,
  });
};
export const setSignupRePass = (signup_re_pass) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_RE_PASS,
    payload: signup_re_pass,
  });
};
export const setSignupAddressArea = (signup_address_area) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_ADDRESS_AREA,
    payload: signup_address_area,
  });
};
export const setSignupAddressBlock = (signup_address_block) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_ADDRESS_BLOCK,
    payload: signup_address_block,
  });
};
export const setSignupAddressRoad = (signup_address_road) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_ADDRESS_ROAD,
    payload: signup_address_road,
  });
};
export const setSignupAddressBuilding = (signup_address_building) => (dispatch) => {
  dispatch({
    type: SET_SIGNUP_ADDRESS_BUILDING,
    payload: signup_address_building,
  });
};

// ________________________
export const setOTP = (otp) => (dispatch) => {
  dispatch({
    type: SET_OTP,
    payload: otp,
  });
};


export const setIsChecked = (is_checked) => (dispatch) => {
  dispatch({
    type: IS_CHECKED,
    payload: is_checked,
  });
};

export const setIsAlreadyBooked = (is_already_booked) => (dispatch) => {
  dispatch({
    type: IS_ALREADY_BOOKED,
    payload: is_already_booked,
  });
};

export const setProfilePic = (pic) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_PIC,
    payload: pic,
  });
 
};