import { debugLog } from '../../common/Constants';
import { insertAppLanguageContent, selectAllRecord } from '../../database/AppDatabase';
import {
  SET_APP_CONTENTS,
  SET_BOOKING_CANCEL_STATUS,
  SET_BOOKING_STATUS,
  SET_CANCEL_BOOKING,
  SET_IS_READY,
  SET_SEARCH_INPUT,
  SET_SHOW_CPR,
  SET_SHOW_PASS_LOGIN,
  SET_SHOW_PASS_REG,
  SET_SHOW_RE_PASS,
  SET_TOGGLE_CHANGE
} from './typesJaswant';


export const setAppLanguage = () => (dispatch) => {
  insertAppLanguageContent().then(() => {
    console.log('Content inserted!');
  });
};

export const getContent = (word) => (dispatch) => {
  selectAllRecord(`app_language_content`).then((res) => {
    // console.log(res);
    dispatch({
      type: SET_APP_CONTENTS,
      payload: res,
    });
  });
};


export const getAllName_db = (table_name, word) => (dispatch) => {
  selectAllRecord(`SELECT * FROM ${table_name} where service_name like '%${word}%'`).then((res) => {
    console.log(res);
    dispatch({
      type: SERVICE_NAME_LIST,
      payload: res,
    });
  });
};

export const setIsReady = (is_ready) => (dispatch) => {
  dispatch({
    type: SET_IS_READY,
    payload: is_ready,
  });
};

// LOGIN
export const setShowPass = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_SHOW_PASS_LOGIN,
    payload: isTrue,
  });
};

// SIGNUP
export const setShowPassRegist = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_SHOW_PASS_REG,
    payload: isTrue,
  });
};

export const setShowRePass = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_SHOW_RE_PASS,
    payload: isTrue,
  });
};

export const setShowCPR = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_SHOW_CPR,
    payload: isTrue,
  });
};


// home screen

export const setSearchInput = (input) => (dispatch) => {
  dispatch({
    type: SET_SEARCH_INPUT,
    payload: input,
  });
};

export const setToggleChange = (isTrue) => (dispatch) => {
  // debugLog(isTrue)
  dispatch({
    type: SET_TOGGLE_CHANGE,
    payload: isTrue,
  });
};

export const setCancelBooking = (isTrue) => (dispatch) => {
  // debugLog(isTrue)
  dispatch({
    type: SET_CANCEL_BOOKING,
    payload: isTrue,
  });
};

export const setBookingStatus = (booking_status) => (dispatch) => {
  // debugLog(isTrue)
  dispatch({
    type: SET_BOOKING_STATUS,
    payload: booking_status,
  });
};

export const setBookingCancelStatus = (bookin_cancel_status) => (dispatch) => {
  // debugLog(isTrue)
  dispatch({
    type: SET_BOOKING_CANCEL_STATUS,
    payload: bookin_cancel_status,
  });
};