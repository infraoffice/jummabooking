import {


} from '../actions/types';
import {
  SET_APP_CONTENTS,
  SET_BOOKING_CANCEL_STATUS,
  SET_BOOKING_STATUS,
  SET_CANCEL_BOOKING,
  SET_IS_READY,
  SET_SEARCH_INPUT,
  SET_SHOW_CPR,
  SET_SHOW_PASS_LOGIN,
  SET_SHOW_PASS_REG,
  SET_SHOW_RE_PASS,
  SET_TOGGLE_CHANGE
} from '../actions/typesJaswant';

const initialState = {
  app_content_list: [],
  is_ready: false,

  show_pass_login: false,
  show_pass_rag: false,
  show_cpr_num: false,
  show_re_enter_pass: false,

  search_input: '',
  is_on: false,
  isBookingCancel: true,

  // Home Screen


  masjid_list: [
    {
      name: 'Masjid Name',
      address: 'Masjid address',
      time: 'time',
      totalBooking: 100,
    },
    {
      name: 'Masjid Name',
      address: 'Masjid address',
      time: 'time',
      totalBooking: 11,
    },
    {
      name: 'Masjid Name',
      address: 'Masjid address',
      time: 'time',
      totalBooking: 47,

    },
    {
      name: 'Masjid Name',
      address: 'Masjid address',
      time: 'time',
      totalBooking: 100,
    },
    {
      name: 'Masjid Name',
      address: 'Masjid address',
      time: 'time',
      totalBooking: 79,
    },
    {
      name: 'Masjid Name',
      address: 'Masjid address',
      time: 'time',
      totalBooking: 10,
    },
    {
      name: 'Masjid Name',
      address: 'Masjid address',
      time: 'time',
      totalBooking: 99,
    },
    {
      name: 'Masjid Name',
      address: 'Masjid address',
      time: 'time',
      totalBooking: 4,
    },
  ],

  booking_satus:
  {

  },

  booking_cance_satus: {

  }

};

export default function (state = initialState, action) {
  switch (action.type) {

    case SET_APP_CONTENTS:
      return {
        ...state,
        app_content_list: action.payload,
      };

    case SET_IS_READY:
      return {
        ...state,
        is_ready: action.payload,
      };

    case SET_SHOW_PASS_LOGIN:
      return {
        ...state,
        show_pass_login: action.payload,
      };

    case SET_SHOW_CPR:
      return {
        ...state,
        show_cpr_num: action.payload,
      };

    case SET_SHOW_PASS_REG:
      return {
        ...state,
        show_pass_rag: action.payload,
      };

    case SET_SHOW_RE_PASS:
      return {
        ...state,
        show_re_enter_pass: action.payload,
      };

    case SET_SEARCH_INPUT:
      return {
        ...state,
        search_input: action.payload,
      };

    case SET_TOGGLE_CHANGE:
      return {
        ...state,
        is_on: action.payload,
      };

    case SET_CANCEL_BOOKING:
      return {
        ...state,
        isBookingCancel: action.payload,
      };

    case SET_BOOKING_STATUS:
      return {
        ...state,
        booking_satus: action.payload,
      };

    case SET_BOOKING_CANCEL_STATUS:
      return {
        ...state,
        booking_cance_satus: action.payload,
      };



    default:
      return state;
  }
}
