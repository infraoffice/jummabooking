import { combineReducers } from 'redux';
import indexAkshitaReducer from './indexAkshitaReducer ';
import indexLangReducer from './indexLangReducer';
import indexReducer from './indexReducer';
import indexReducerJaswant from './indexReducerJaswant';

export default combineReducers({
    indexReducer: indexReducer,
    indexReducerJaswant: indexReducerJaswant,
    indexLangReducer:indexLangReducer,
    indexAkshitaReducer:indexAkshitaReducer

})

