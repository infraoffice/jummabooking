
import { IS_ALREADY_BOOKED, IS_CHECKED, OTP_FIRST, OTP_FOURTH, OTP_SECOND, OTP_THIRD, SET_LOGIN_MOBILE, SET_LOGIN_PASS, SET_OTP, SET_PROFILE_PIC, SET_SIGNUP_ADDRESS_AREA, SET_SIGNUP_ADDRESS_BLOCK, SET_SIGNUP_ADDRESS_BUILDING, SET_SIGNUP_ADDRESS_ROAD, SET_SIGNUP_CPR_NO, SET_SIGNUP_MOBILE, SET_SIGNUP_NAME, SET_SIGNUP_PASS, SET_SIGNUP_RE_PASS } from '../actions/typesAkshita';

const initialState = {
  is_loading: false,
  // login var

  login_mobile: '',
  login_pass: '',

  // signup var
  signup_name: '',
  signup_cpr_no: '',
  signup_mobile: '',
  signup_pass: '',
  signup_re_pass: '',
  signup_address_area: '',
  signup_address_block: '',
  signup_address_road: '',
  signup_address_building: '',


  otp: '',

  is_checked: '',

  is_already_booked: false,
  set_profile: '',

};

export default function (state = initialState, action) {
  switch (action.type) {

    case SET_LOGIN_MOBILE:
      return {
        ...state,
        login_mobile: action.payload,
      };
    case SET_LOGIN_PASS:
      return {
        ...state,
        login_pass: action.payload,
      };


    case SET_SIGNUP_NAME:
      return {
        ...state,
        signup_name: action.payload,
      };
    case SET_SIGNUP_CPR_NO:
      return {
        ...state,
        signup_cpr_no: action.payload,
      };
    case SET_SIGNUP_MOBILE:
      return {
        ...state,
        signup_mobile: action.payload,
      };
    case SET_SIGNUP_PASS:
      return {
        ...state,
        signup_pass: action.payload,
      };
    case SET_SIGNUP_RE_PASS:
      return {
        ...state,
        signup_re_pass: action.payload,
      };
    case SET_SIGNUP_ADDRESS_AREA:
      return {
        ...state,
        signup_address_area: action.payload,
      };
    case SET_SIGNUP_ADDRESS_BLOCK:
      return {
        ...state,
        signup_address_block: action.payload,
      };
    case SET_SIGNUP_ADDRESS_ROAD:
      return {
        ...state,
        signup_address_road: action.payload,
      };
    case SET_SIGNUP_ADDRESS_BUILDING:
      return {
        ...state,
        signup_address_building: action.payload,
      };

    case SET_OTP:
      return {
        ...state,
        otp: action.payload,
      };

    case IS_CHECKED:
      return {
        ...state,
        is_checked: action.payload,
      };

    case IS_ALREADY_BOOKED:
      return {
        ...state,
        is_already_booked: action.payload,
      };

      case SET_PROFILE_PIC:
        return {
          ...state,
          set_profile: action.payload,
        };
  
    default:
      return state;
  }
}