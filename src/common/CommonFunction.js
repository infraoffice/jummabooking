import moment from 'moment/moment';

export function calc_age(inputDate) {
    let formatted_date = moment(new Date(inputDate));
    let today_date = moment(new Date());

    let years = today_date.diff(formatted_date, 'years');
    let months = today_date.diff(formatted_date, 'months');

    let diff_years = years;
    let diff_months = months - (years * 12);

    return diff_years + " Years " + diff_months + " months ";
}

export function is_date_expired(inputDate) {
    if (inputDate === null || inputDate === '') return false;
    let formatted_date = moment(new Date(inputDate));
    let today_date = moment(new Date());
    return today_date.isAfter(formatted_date);
}
