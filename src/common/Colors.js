const redColor = '#CE1126'
const whiteColor = '#FFFFFF'
const greenColor = '#1BB507'
const lightGreenColor = '#C3ECBE'
const textColor = '#AAB1BB'
const headingColor = '#343434'
const backgroundColor = '#F3F5F9'
const borderColor='#F3F5F9'

export default {
  redColor,
  whiteColor,
  greenColor,
  lightGreenColor,
  textColor,
  headingColor,
  backgroundColor,
  borderColor,
};
