import React from 'react'
import { Image, StyleSheet, Text, TextInput, View } from 'react-native';
import Colors from '../common/Colors';
const SearchInput = (props) => {
    return (
        <View style={[styles.mainStyle, props.style]}>

            <View style={styles.timeRow} >
                <View style={styles.dot} />
                <Text style={styles.txtStyle} >
                    {'Next Juma’a on 12 Dec  at 11:30 AM'}
                </Text>
            </View>

            <View style={[styles.inputStyle,styles.myshadow,props.inputStyle]} >
                <TextInput
                    placeholder='Search for Masjid'
                    onChangeText={props.onChangeText}
                    style={{ flex: 1, backgroundColor: "white", }}
                />
                <Image
                    source={require("../../assets/image/search.png")}
                    style={{ height: 25, width: 25, resizeMode: "contain" }}
                />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    inputStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "white",
        elevation: 5,
        margin: 10,
        paddingHorizontal: 5,
        borderWidth: 1,
        borderColor: Colors.textColor,
        borderRadius: 8,
        height: 50
    },
    mainStyle: {
        alignItems: "center",
        backgroundColor:'#0000'
    },
    dot: {
        width: 10,
        height: 10,
        backgroundColor: Colors.redColor,
        borderRadius: 30,
        marginHorizontal: 5
    },
    timeRow: {
        flexDirection: "row",
        alignItems: 'center',

    },
    txtStyle: {
        fontSize: 14,
        color: Colors.headingColor,
        fontStyle: 'normal',
        fontWeight: '500',

    },
    myshadow: {
        shadowColor: 'rgba(95, 90, 247, 0.03)',
        shadowOffset: {
            height: 1,
            width: 0,
        },
        shadowOpacity: 0.5,
        elevation: 11,
    },
})

export default SearchInput;