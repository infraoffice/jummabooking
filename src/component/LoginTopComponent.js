import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';

const LoginTopComponent = (props) => {
  return (
    <View style={styles.container}>

      <TouchableOpacity onPress={props.onAR}>
        <Text style={styles.txtStyle}>{'AR/'} </Text>
      </TouchableOpacity >

      <TouchableOpacity onPress={props.onEN}>
        <Text style={styles.txtStyle}>{'EN/'} </Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={props.onUR}>
        <Text style={styles.txtStyle}>{'UR/'} </Text>
      </TouchableOpacity>
      
      <TouchableOpacity onPress={props.onBN}>
        <Text style={styles.txtStyle}>{'BN'} </Text>
      </TouchableOpacity>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 48,
    // backgroundColor: '#448AFF',
    marginTop: 20,
    flexDirection: 'row',
    alignSelf: 'flex-end',
    marginRight: 10
  },

  txtStyle: {
    fontSize: 16,
    fontWeight: 'bold'
  }
});
export default LoginTopComponent;
