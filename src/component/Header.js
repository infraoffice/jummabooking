import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';

const Header = (props) => {
  return (
    <TouchableOpacity style={[{
      backgroundColor: Colors.backgroundColor,
      height: 40,
      padding: 5,
      flexDirection: "row",
      justifyContent: "flex-start",
      alignItems: "center"
    }, props.style]}>
      <TouchableOpacity
        onPress={props.onPress}
        style={{ height: 48, width: 48, justifyContent: "center" }}
      >
        {props.isMenu ?
          <Image
            style={{ height: 12, width: 18, marginLeft: 10, }}
            source={require('../../assets/image/menu.png')}
          />
          : <Image
            style={{ height: 12, width: 18, marginLeft: 10 }}
            source={require('../../assets/image/back.png')}
          />}
      </TouchableOpacity>
      <Text style={{ fontSize: 20, fontStyle:'normal',color:Colors.headingColor, fontWeight: 'bold',  }}>{props.title} </Text>

    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 48,
    backgroundColor: Colors.whiteText,
    flexDirection: 'row',
    alignItems: 'center',
  },
  imgStyle: {
    height: getWidth(40),
    width: getWidth(40)
  },

});
export default Header;
