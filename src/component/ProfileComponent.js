import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';

const ProfileComponent = (props) => {
  return (
    <TouchableOpacity style={styles.main}>
      <View style={{
        width: 84,
        height: 84, borderRadius: 100,
        borderWidth: 2, borderColor: 'rgba(206, 17, 38, 0.09)',
        justifyContent: "center", alignItems: 'center',
        marginLeft: 19,
        marginRight: 7
      }}>
        <View style={{
          width: 74,
          height: 74, borderRadius: 100,
          borderWidth: 2, borderColor: 'rgba(206, 17, 38, 0.4)',
          justifyContent: "center", alignItems: 'center'
        }}>
          <Image
            source={props.profile}
            style={{ height: 62, width: 62 }}
          />
        </View>
      </View>
      <View>
        <Text style={styles.txtStyle}>{props.name} </Text>
        <Text style={[styles.txtStyle,]}>{props.status} </Text>

      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  main: {
    backgroundColor: Colors.backgroundColor,
    height: 130,
    padding: 5,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  imgStyle: {
    height: getWidth(40),
    width: getWidth(40)
  },
  txtStyle: {
    color: Colors.textColor,
    fontFamily: Font.SourceSansPro,
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize:16
  }
});
export default ProfileComponent;
