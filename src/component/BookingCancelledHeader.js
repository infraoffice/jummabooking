import React from 'react';
import { View, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';
import SearchInput from './SearchInput';

const BookingCancelledHeader = (props) => {
  return (
    <View style={{
      height: getWidth(160),
      paddingVertical: 5,
      backgroundColor: Colors.whiteColor,
      borderTopLeftRadius: 20,
      borderTopRightRadius: 20,
      elevation: 10,
      paddingVertical: 5,
      marginTop:-55,
    }} >


      {/* <View style={styles.timeRow} >
        <View style={styles.dot} />
        <Text style={styles.txtStyle} >
          {'Next Juma’a on 12 Dec  at 11:30 AM'}
        </Text>
      </View>


      <View style={[styles.inputStyle, props.inputStyle]} >
        <TextInput
          placeholder={props.placeholder}
          onChangeText={props.onChangeText}
          style={{
            flex: 1,
            backgroundColor: "white",
            color: Colors.headingColor,
            fontFamily: Font.Roboto,
            fontFamily: Font.Roboto,
            fontWeight: '400',
            fontStyle: 'normal'
          }}
        />
        <Image
          source={require("../../assets/image/search.png")}
          style={{ height: 25, width: 25, resizeMode: "contain" }}
        />
      </View> */}

      <View style={[{
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#F9D9DC',
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        elevation: 15,
        height: 142,
        paddingVertical: 10
      }, props.style]}>

        <View style={{ marginRight: 20, marginLeft: 30, flex: 1 }}>

          <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }} >
            <Text style={{ fontSize: 12, color: Colors.redColor, fontFamily: Font.Roboto }} >{"Masjid Name"} </Text>

            <TouchableOpacity
              onPress={props.onCancel}
              style={[{
              }]}
            >
              <Image
                source={require("../../assets/image/cancel.png")}
                style={{ height: 25, width: 25, }}
              />
            </TouchableOpacity>
          </View>

          <Text style={{ paddingVertical: 5, fontWeight: '400', color: Colors.headingColor, fontSize: 10 }} >
            {props.booking_cancel_reason} </Text>
          <Text style={{ fontSize: 10, color: Colors.redColor, fontFamily: Font.Roboto }} >{"- Corona Virus found there"} </Text>
          <Text style={{ fontSize: 10, color: Colors.headingColor, fontWeight: '400', }} >{props.book_again} </Text>

        </View>

      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 48,
    backgroundColor: Colors.whiteText,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: "#0000"
  },
  imgStyle: {
    height: getWidth(40),
    width: getWidth(40)
  },
  txtStyle: {
    fontSize: 14,
    color: Colors.headingColor,
    fontStyle: 'normal',
    fontWeight: '500',
  },
  inputStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: "white",
    elevation: 1,
    margin: 10,
    marginHorizontal: 30,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: Colors.textColor,
    borderRadius: 8,
    height: 40
  },
  btnStyle: {
    borderRadius: 30,
    backgroundColor: Colors.redColor,
    elevation: 2,
    width: 73,
    height: 23,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  dot: {
    width: 10,
    height: 10,
    backgroundColor: Colors.redColor,
    borderRadius: 30,
    marginHorizontal: 5
  },
  timeRow: {
    flexDirection: "row",
    alignItems: 'center',
    // alignSelf: 'center',
    marginLeft: 25

  },
  txtStyle: {
    fontSize: 14,
    color: Colors.headingColor,
    fontStyle: 'normal',
    fontWeight: '500',

  },
  btnTxtStyle: {
    fontSize: 10,
    fontStyle: 'normal',
    fontWeight: 'normal',
    color: Colors.whiteColor,
  }
});
export default BookingCancelledHeader;
