import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';

const ConfBooking = (props) => {
    return (
        <View style={styles.container} >

            <Text style={styles.txt1}>
                {'Ahmad Masjid'}
            </Text>
            <Text style={styles.txt2}>
                {'Awal Avenue Corner AI Faith Highway,Bahrain'}
            </Text>


            <View style={styles.row1}>
                <Text style={styles.txt3}>{'1.3 Km away'} </Text>
                <TouchableOpacity
                    onPress={props.onPress}
                >
                    <Text
                        style={styles.txt4}>
                        {props.get_details}
                    </Text>
                </TouchableOpacity>
            </View>


            <View style={styles.row2}>
                <View style={styles.date}>
                    <Text style={styles.dateTxt}>{'Dec 12,2020'} </Text>
                </View>
                <View style={{ width: 2, backgroundColor: Colors.textColor, height: 40 }} />
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={styles.timeTxt}>{'11:30 AM'} </Text>
                </View>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 24,
        elevation: 5,
        alignSelf: 'center',
        backgroundColor: Colors.whiteColor,
        borderRadius: 10,
        height: 150, width: getWidth(300)
    },
    txt1: {
        fontWeight: '700',
        fontFamily: Font.Roboto,
        marginLeft: 26,
        marginTop: 15,
        fontSize: 16,
        color: Colors.headingColor
    },
    txt2: {
        fontWeight: '400',
        fontFamily: Font.Roboto,
        marginTop: 5,
        width: 180,
        fontSize: 14,
        color: Colors.textColor,
        marginLeft: 26
    },
    row1: {
        flexDirection: 'row',
        marginTop: 5,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    txt3: {
        fontSize: 14,
        color: Colors.headingColor,
        fontWeight: '500',
        fontFamily: Font.Roboto
    },
    txt4: {
        fontSize: 16,
        color: Colors.greenColor,
        fontWeight: '400',
        fontFamily: Font.Roboto
    },
    row2: {
        flexDirection: 'row',
        marginTop: 5,
        flex: 1,
        borderTopWidth: 1,
        borderColor: Colors.textColor,
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    date: {

        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: Colors.textColor
    },
    dateTxt: {
        fontSize: 16,
        fontWeight: '500',
        color: Colors.headingColor,
        fontFamily: Font.Roboto
    },
    timeTxt: {
        fontFamily: Font.Roboto,
        fontSize: 16,
        fontWeight: '500',
        color: Colors.headingColor
    },
})

export default ConfBooking;