import { TouchableOpacity, Text, View, StyleSheet, Image } from 'react-native';
import React from 'react';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';

const BottomNev = (props) => {
  return (
      <View style={styles.main}>
        <TouchableOpacity
          style={[styles.leftView, styles.myshadow]}
          onPress={props.onPressHome}>
          <View style={[styles.activeStyle, props.left_label != "" ? null : { backgroundColor: "white" }]} >
 
              <Image
                style={{ width: 25, height: 25, resizeMode: 'contain' }}
                source={
                  props.homeIcon != null
                    ? props.homeIcon
                    : require('../../assets/image/home.png')
                }
              />
            {
              props.left_label != "" ?
                <Text style={styles.btnTxtStyle} >{props.left_label}</Text>
                : null
            }
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          style={[styles.addView]}
          onPress={props.onPressQR}>
          <Image
            style={{
              width: 25, height: 25,
              resizeMode: 'contain'
            }}
            source={props.qrImage}
          />
        </TouchableOpacity>

        <TouchableOpacity
          style={[styles.rightView, styles.myshadow]}
          onPress={props.onPressHelp}>
          <View style={[styles.activeStyle, props.right_label != "" ? null : { backgroundColor: "white" }]} >
      
            <Image
              style={{ width: 30, height: 30 }}
              borderRadius={15}
              source={
                props.helpIcon != null
                  ? props.helpIcon
                  : require('../../assets/image/help.png')
              }
            />
            {
              props.right_label != "" ?
                <Text style={styles.btnTxtStyle} >{props.right_label}</Text>
                : null
            }
          </View>
        </TouchableOpacity>
      </View>
  );
};

const styles = StyleSheet.create({
  main: {
    height: 44,
    width:'100%',
    position:'absolute',
    elevation:25,
    bottom:0,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor:Colors.whiteColor,
    justifyContent:'flex-end'
  },
  leftView: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    height: '100%',
    justifyContent: 'center',
    borderTopRightRadius: 30,
  },
  addView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 45,
    borderRadius: 30,
    elevation:10,
    backgroundColor:Colors.redColor,
    height:50,
    width:50
  },
  rightView: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    height: '100%',
    justifyContent: 'center',
    borderTopLeftRadius: 30,
  },
  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,

    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 13,
  },
  activeStyle: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    backgroundColor: `rgba(16, 80, 230, 0.04)`,
    width: getWidth(120),
    height: 35,
    borderRadius: 20
  },
  btnTxtStyle: {
    color: "#1050E6",
    marginRight: 15,
  }
});
export default BottomNev;
