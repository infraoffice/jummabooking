import React from 'react';
import { View, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';
import Colors from '../common/Colors';

const ListHeader = (props) => {
  return (
    <View style={{
      height: 33,
      borderTopRightRadius: 20,
      borderTopLeftRadius: 20,
      backgroundColor: '#F3F5F9',
      justifyContent: 'center',

    }} >
      <View style={{
        marginLeft: 30,
        marginRight: 20,
        justifyContent: "space-between",
        alignItems: 'center',
        flexDirection: 'row',
      }} >
        <Text style={{ color: Colors.textColor, fontSize: 16 }} >{props.location} </Text>
        <View style={{ flexDirection: 'row', alignItems: "center", }} >
          <View style={[styles.dot, { backgroundColor: Colors.greenColor }]} />
          <Text style={styles.txtStyle} >{props.available} </Text>
          <View style={styles.dot} />
          <Text style={styles.txtStyle}>{props.booked} </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  dot: {
    width: 10,
    height: 10,
    backgroundColor: Colors.redColor,
    borderRadius: 30,
    marginHorizontal: 5
  },
  txtStyle: {
    fontSize: 10,
    color: Colors.textColor
  }
});
export default ListHeader;
