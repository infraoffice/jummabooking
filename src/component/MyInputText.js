import { TouchableOpacity, Text, StyleSheet, View, Image, TextInput } from 'react-native';
import React from 'react';
import Font from '../common/Font';
import Colors from '../common/Colors';

const MyInputText = (props) => {
  return (
    <View style={{ flexDirection: "row", alignItems: "center", paddingHorizontal: 5 }} >
      <Image
        source={props.icon}
        style={[{ width: 13, height: 18 }, props.iconStyle]}
      />
      <TextInput
        placeholder={props.placeholder}
        keyboardType={props.keyboardType}
        maxLength={props.maxLength}
        style={{ flex: 1, marginLeft: 30, fontSize: 16 }}
        secureTextEntry={props.secureTextEntry}
      />

      <TouchableOpacity
        onPress={props.onPress}
        style={{ height: 45, justifyContent: "center", }}
      >
        <Text style={styles.txt} >
          {props.right_text}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  mainStyle: {
    alignItems: 'center',
    height: 45,
    paddingHorizontal: 10
  },
  rowStyle: {
    flexDirection: 'row',
    borderBottomColor: Colors.themeColor,
    borderBottomWidth: 1,
    marginLeft: 39,
    marginRight: 120,
    justifyContent: 'center',
    alignItems: 'center',
    height: 51,

  },
  txt: {
    color: Colors.textColor,
    fontSize: 14,
    fontStyle: "normal",
    fontWeight: '500',
    paddingHorizontal:10,
  },
});

export default MyInputText;