import { TouchableOpacity, Text, StyleSheet, View, Image, TextInput } from 'react-native';
import React from 'react';
import Font from '../common/Font';
import Colors from '../common/Colors';

const MyInput = (props) => {
  return (
    <TextInput
      onChangeText={props.onChangeText}
      placeholder={props.placeholder}
      value={props.value}
      keyboardType={props.keyboardType}
      maxLength={props.maxLength}
      secureTextEntry={props.secureTextEntry}
      style={[{
        borderBottomWidth: 1,
        borderBottomColor: Colors.themeColor
      }, props.style]}
    />
  );
};

const styles = StyleSheet.create({
  mainStyle: {
    alignItems: 'center',
    height: 50,
    paddingHorizontal: 10
  },

});

export default MyInput;