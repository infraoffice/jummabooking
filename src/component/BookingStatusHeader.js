import React from 'react';
import { View, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';
import SearchInput from './SearchInput';

const BookingStatusHeader = (props) => {
  return (
    <View style={{
      height: getWidth(160),
      paddingVertical: 5,
      backgroundColor: Colors.whiteColor,
      borderTopLeftRadius: 20,
      borderTopRightRadius: 20,
      elevation: 10,
      marginTop: -55,
    }} >


      {/* <View style={styles.timeRow} >
        <View style={styles.dot} />
        <Text style={styles.txtStyle} >
          {'Next Juma’a on 12 Dec  at 11:30 AM'}
        </Text>
      </View>


      <View style={[styles.inputStyle, props.inputStyle]} >
        <TextInput
          placeholder={props.placeholder}
          onChangeText={props.onChangeText}
          style={{ flex: 1, backgroundColor: "white", color: Colors.headingColor, fontFamily: Font.Roboto, fontWeight: '400', fontStyle: 'normal' }}
        />
        <Image
          source={require("../../assets/image/search.png")}
          style={{ height: 25, width: 25, resizeMode: "contain" }}
        />
      </View> */}


      <View style={[{
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: Colors.lightGreenColor,
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingHorizontal: 30,
        elevation: 15,
        height: 142,
        paddingVertical: 10

      }, props.style]}>

        <View>
          <Text style={{ fontSize: 12, color: Colors.headingColor, fontFamily: Font.Roboto }}>{props.you_have_booked} </Text>
          <Text style={{ fontSize: 16, color: Colors.greenColor, fontFamily: Font.Roboto }} >{"Masjid Name"} </Text>
          <Text style={{ width: 156, paddingVertical: 5, fontWeight: '400', color: Colors.headingColor, fontSize: 10 }} >{"Address"} </Text>
          <Text style={{ fontSize: 12, color: Colors.headingColor, fontWeight: '500', }} >{"Distence"} </Text>

        </View>

        <View style={{
          paddingVertical: 10
        }} >
          <TouchableOpacity
            onPress={props.onCancelBookning}
            style={styles.btnStyle}
          >
            <Text style={styles.btnTxtStyle} >
              {props.cancel_booking}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[styles.btnStyle, {
              borderWidth: 1,
              borderColor: Colors.greenColor,
              backgroundColor: Colors.whiteColor,

            }]}
          >
            <Text style={[styles.btnTxtStyle, { color: Colors.greenColor, fontSize: 10 }]} >
              {props.navigate_now}
            </Text>
          </TouchableOpacity>
        </View>


      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 48,
    backgroundColor: Colors.whiteText,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: "#0000"
  },
  imgStyle: {
    height: getWidth(40),
    width: getWidth(40)
  },
  txtStyle: {
    fontSize: 14,
    color: Colors.headingColor,
    fontStyle: 'normal',
    fontWeight: '500',
  },
  inputStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: "white",
    elevation: 1,
    margin: 10,
    marginHorizontal: 30,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: Colors.textColor,
    borderRadius: 8,
    height: 40
  },
  btnStyle: {
    borderRadius: 11,
    backgroundColor: Colors.redColor,
    elevation: 2,
    width: 100,
    height: 23,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  dot: {
    width: 10,
    height: 10,
    backgroundColor: Colors.redColor,
    borderRadius: 30,
    marginHorizontal: 5
  },
  timeRow: {
    flexDirection: "row",
    alignItems: 'center',
    // alignSelf: 'center',
    marginLeft: 25

  },
  txtStyle: {
    fontSize: 14,
    color: Colors.headingColor,
    fontStyle: 'normal',
    fontWeight: '500',

  },
  btnTxtStyle: {
    fontSize: 10,
    fontStyle: 'normal',
    fontWeight: 'normal',
    color: Colors.whiteColor,
    textAlign: 'center'
  }
});
export default BookingStatusHeader;
