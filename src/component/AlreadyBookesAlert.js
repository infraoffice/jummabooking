import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';
import CustomeButton from './CustomeButton';

const AlreadyBookesAlert = (props) => {
  return (


    <View style={[{
      backgroundColor: Colors.whiteColor,
      height: getWidth(330),
      width: getWidth(360),
      // justifyContent: 'space-between',
      backgroundColor: Colors.whiteColor,
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingHorizontal: 20,

    }, props.style]}>


      <View style={{ flexDirection: 'row', marginTop: 40, marginHorizontal: 30, alignItems: 'center', justifyContent: 'center' }}>
        <TouchableOpacity style={{ alignItems: 'center' }}>
          <Image
            style={{ height: getWidth(31), width: getWidth(31), }}
            source={require('../../assets/image/alredy_booked.png')}
          />
        </TouchableOpacity>

        <View style={{ marginLeft: 8 }}>
          <Text style={styles.txtStyle}>{'Sorry !'} </Text>
          <Text style={[styles.txtStyle, { fontSize: 14, fontWeight: '400' }]}>{'you can’t book two location for the same Salah'} </Text>
        </View>
      </View>

      <View style={{ height: 1, marginTop: 10, backgroundColor: '#F3F5F9',marginVertical:20,marginHorizontal:20, }} />

      <Text style={[styles.txtStyle, { marginHorizontal: 20, fontSize: 16, color: Colors.textColor }]}>
        {'You are already booked a location in Ahmad Masjid for Juma’a Salah on 12-Dec-2020 at 11:30 AM'}
      </Text>

      <CustomeButton
        onPress={props.onOk}
        title="OK"
        height={getWidth(47)}
        width={getWidth(141)}
        alignSelf="center"
        style={{
          marginTop: 29,
          backgroundColor: Colors.redColor,
          elevation: 10,
          borderRadius: 10,
          justifyContent: 'flex-end',
        }}
        txtStyle={{ color: Colors.whiteColor, fontFamily: Font.Roboto, fontSize: 16, fontWeight: '500', marginRight: getWidth(20) }}
        iconVisible={true}
        image={require('../../assets/image/arrow.png')}
        imageStyle={{
          marginRight: 10, height: 30, width: 30,
          backgroundColor: '#AB1126',
        }}
      />


    </View>

  );
};

const styles = StyleSheet.create({
  // container: {
  //   height: 48,
  //   backgroundColor: Colors.whiteText,
  //   flexDirection: 'row',
  //   alignItems: 'center',
  //   backgroundColor: "#0000"
  // },
  // imgStyle: {
  //   height: getWidth(40),
  //   width: getWidth(40)
  // },
  txtStyle: {
    fontSize: 16,
    color: Colors.redColor,
    fontStyle: 'normal',
    fontFamily: Font.Roboto,
    fontWeight: '500',
  },
});
export default AlreadyBookesAlert;



