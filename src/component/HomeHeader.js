import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';

const HomeHeader = (props) => {
  return (
    <View style={[{
      backgroundColor: Colors.backgroundColor,
      height: 50,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      backgroundColor: "#0000"
    }, props.style]}>

      <TouchableOpacity
        onPress={props.onPressMenu}
        style={{ height: 48, width: 48, justifyContent: "center", }}
      >
        <Image
          style={{ height: 12, width: 18, marginLeft: 10 }}
          source={require('../../assets/image/menu.png')}
        />
      </TouchableOpacity>

      <View style={{ flexDirection: "row", justifyContent: 'center', alignItems: 'center', }} >
        <Text style={styles.txtStyle} > {props.toggleText}</Text>
        <TouchableOpacity
          onPress={props.onToggleChange}
        // style={{ height: 48, width: 48, justifyContent: "center", }}
        >
          <Image
            style={{ height: 20, width: 25, marginHorizontal: 10 }}
            source={props.is_on ? require("../../assets/image/on.png") : require("../../assets/image/off.png")}
          />

        </TouchableOpacity>

        <TouchableOpacity
          onPress={props.onAlert}
          style={{ flexDirection: "row", marginRight: 30, }} >
          <Image
            style={{ height: 20, width: 15, marginLeft: 10 }}
            source={require('../../assets/image/alert.png')}
          />
          <View style={styles.dot} />
        </TouchableOpacity>

      </View>


    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 48,
    backgroundColor: Colors.whiteText,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: "#0000"
  },
  imgStyle: {
    height: getWidth(40),
    width: getWidth(40)
  },
  txtStyle: {
    fontSize: 14,
    color: Colors.headingColor,
    fontStyle: 'normal',
    fontWeight: '500',
  },
  dot: {
    width: 6,
    height: 6,
    backgroundColor: Colors.redColor,
    borderRadius: 30,
    marginTop: 4,
    marginLeft: -5
  },
});
export default HomeHeader;
