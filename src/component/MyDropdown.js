import React, { Component } from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  Image,
  StyleSheet,
  FlatList,
  SafeAreaView, Dimensions, Platform
} from 'react-native';
import Colors from '../common/Colors';
import { debugLog } from '../common/Constants';
import { getWidth } from '../common/Layout';


class MyDropdown extends Component {
  constructor(props) {
    super(props);
  };
  render() {
    return (
      <View style={[Platform.OS == "ios" ? styles.iosStyle : styles.androidStyle, this.props.style]}>
        <TouchableOpacity
          onPress={this.props.isVisible}
          style={[styles.TouchableStyle, this.props.ItemStyle]}>
          <View style={styles.locationStyle}>
            {
              this.props.left_icon != null ?
                <Image
                  source={this.props.left_icon}
                  style={{
                    width: 20,
                    height: 20,
                    marginRight: 17.55,
                    resizeMode: 'contain',
                    
                  }}
                /> : null
            }

            <Text style={{ color: Colors.textColor, }}>
              {this.props.placeholder}
            </Text>
            <View style={this.props.seperatorStyle}/>
          </View>
          <TouchableOpacity onPress={this.props.isVisible}>
            <Image
              source={
                this.props.isDropdownEnabled
                  ? require('../../assets/image/drop_up.png')
                  : require('../../assets/image/drop_g.png')
              }
              style={[{ width: 15, height: 15, resizeMode: 'contain' },this.props.iconStyle]}
            />
          </TouchableOpacity>
        </TouchableOpacity>

        {this.props.isDropdownEnabled ? (
          <View style={[{
            backgroundColor: "white",

            borderRadius: 2,
            position: "absolute",

            zIndex: 5,
            padding: 5,
            marginTop: 62,
            flex: 1,
            height: 210,
            borderRadius: 10,
            elevation: 13,
            alignSelf: "flex-end",
            borderWidth: 1,
            borderColor: "#e5e5e5"
          }, this.props.listStyle, styles.myshadow]} >
            <FlatList
            showsVerticalScrollIndicator={false}
              data={this.props.dropdown_list}
              renderItem={this.props.renderItem}
              keyExtractor={(item, index) => "key" + item.value}

            />
          </View>
        ) : null}
      </View>
    );
  }
}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({

  TouchableStyle: {
    height: 56,
    backgroundColor: Colors.whiteColor,
    borderBottomColor: Colors.placeHolderColor,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 10,
    padding: 10,
    elevation: 10
  },

  locationStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 13,
  },
  iosStyle: { width: getWidth(200), height: 71, zIndex: 5 },
  androidStyle: { width: getWidth(200), height: 60 },
});
export default MyDropdown;