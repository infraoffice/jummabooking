import { TouchableOpacity, Text, StyleSheet, View, Image, TextInput, Keyboard } from 'react-native';
import React from 'react';
import Font from '../common/Font';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';
import { debugLog } from '../common/Constants';

const SearchMasjid = (props) => {
  return (
    <View style={{
      height: getWidth(140),
      paddingVertical: 5,
      backgroundColor: Colors.whiteColor,
      borderTopLeftRadius: 20,
      borderTopRightRadius: 20,
      elevation: 10
    }} >
      <View style={styles.timeRow} >
        <View style={styles.dot} />
        <Text style={styles.txtStyle} >
          {'Next Juma’a on 12 Dec  at 11:30 AM'}
        </Text>
      </View>


      <View style={[styles.inputStyle, props.inputStyle]} >
        <TextInput
          placeholder={props.placeholder}
          onChangeText={props.onChangeText}
          onFocus={props.onFocus}
          onBlur={props.onBlur}
          style={{ flex: 1, backgroundColor: "white", color: Colors.headingColor, fontFamily: Font.Roboto, fontWeight: '400', fontStyle: 'normal' }}
        />
        <Image
          source={require("../../assets/image/search.png")}
          style={{ height: 25, width: 25, resizeMode: "contain" }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainStyle: {
    alignItems: 'center',
    height: 50,
    paddingHorizontal: 10
  },
  dot: {
    width: 10,
    height: 10,
    backgroundColor: Colors.redColor,
    borderRadius: 30,
    marginHorizontal: 5
  },
  timeRow: {
    flexDirection: "row",
    alignItems: 'center',
    // alignSelf: 'center',
    marginLeft: 25

  },
  txtStyle: {
    fontSize: 14,
    color: Colors.headingColor,
    fontStyle: 'normal',
    fontWeight: '500',

  },
  inputStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: "white",
    elevation: 1,
    margin: 10,
    marginHorizontal: 30,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: Colors.textColor,
    borderRadius: 8,
    height: 40
  },

});

export default SearchMasjid;