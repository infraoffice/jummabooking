import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, ImageBackground } from 'react-native';
import Header from '../component/Header';
import Colors from '../common/Colors'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getWidth } from '../common/Layout';
import Font from '../common/Font';
import ViewMoreText from 'react-native-view-more-text';
 
class MasjidInformationScreen extends Component {
 
 
   renderViewMore(onPress) {
       return (
           <Text style={{ fontWeight: '400', fontSize: 12, fontFamily: Font.Roboto, marginHorizontal: 11, color: Colors.headingColor }}
            onPress={onPress}>{'more...'} </Text>
       )
   }
   renderViewLess(onPress) {
       return (
           <Text style={{ fontWeight: '400', fontSize: 12, fontFamily: Font.Roboto, marginHorizontal: 11, color: Colors.headingColor }}
            onPress={onPress}>{'less'} </Text>
       )
   }

   onBookNow = () => {
    this.props.navigation.navigate("BookingConfirmationScreen")
}

   render() {
       return (
 
           <SafeAreaView style={styles.container}>
               <View style={styles.mainView}>
 
                   <ImageBackground
                       style={{ height: getWidth(197), width: getWidth(360) }}
                       source={require('../../assets/image/masjid.jpg')}
                   >
                       <TouchableOpacity
                           onPress={() => this.props.navigation.goBack()}
                       >
                           <Image
                               style={{ height: 12, width: 18, margin: 20 }}
                               source={require('../../assets/image/white_back.png')}
                           />
                       </TouchableOpacity>
                   </ImageBackground>
 
 
                   <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 65 }}>
                       <Text style={styles.txtStyle1}>
                           {'Capacity'}
                       </Text>
                       <Text style={styles.txtStyle2}>
                           {'150 Person'}
                       </Text>
                   </View>
                   <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20 }}>
                       <Text style={styles.txtStyle1}>
                           {'Booking Status'}
                       </Text>
                       <Text style={styles.txtStyle2}>
                           {'75% Booked'}
                       </Text>
                   </View>
                   <View style={{ height: 1, width: getWidth(324), marginTop: 12, marginHorizontal: 22, backgroundColor: Colors.textColor }} />
 
 
 
 
                   <View style={{ flexDirection: 'row', marginTop: 12, alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20 }}>
                       <Text style={styles.txtStyle1}>
                           {'Imam name'}
                       </Text>
                       <Text style={styles.txtStyle2}>
                           {'Mr.Ibrahim(Hafiz/Qari/Alim)'}
                       </Text>
                   </View>
 
                   <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20 }}>
                       <Text style={styles.txtStyle1}>
                           {'Moazzin name'}
                       </Text>
                       <Text style={styles.txtStyle2}>
                           {'Mr.Ismall'}
                       </Text>
                   </View>
 
                   <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20 }}>
                       <Text style={styles.txtStyle1}>
                           {'Khadim name'}
                       </Text>
                       <Text style={styles.txtStyle2}>
                           {'Mr.Suleman'}
                       </Text>
                   </View>
 
                   <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20 }}>
                       <Text style={styles.txtStyle1}>
                           {'President'}
                       </Text>
                       <Text style={styles.txtStyle2}>
                           {'Mr.Suleman'}
                       </Text>
                   </View>
 
                   <View style={{ height: 1, width: getWidth(324), marginTop: 12, marginHorizontal: 22, backgroundColor: Colors.textColor }} />
                   <View style={{ flexDirection: 'row', marginTop: 12, alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20 }}>
                       <Text style={styles.txtStyle1}>
                           {'Contact number'}
                       </Text>
                       <Text style={styles.txtStyle2}>
                           {'365424565'}
                       </Text>
                   </View>
 
                   <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20 }}>
                       <Text style={styles.txtStyle1}>
                           {'Email'}
                       </Text>
                       <Text style={styles.txtStyle2}>
                           {'abcde@gmail.com'}
                       </Text>
                   </View>
 
 
                   <View style={{ alignSelf: 'center', elevation: 4, borderRadius: 10, height: getWidth(94), marginTop: 20, width: getWidth(294), backgroundColor: Colors.whiteColor, }}>
                       <Text style={[styles.txtStyle1, { fontSize: 14, marginLeft: 10, marginTop: 6 }]}>{'About the Mosque'} </Text>
 
                       <ViewMoreText
                           numberOfLines={2}
                           renderViewMore={this.renderViewMore}
                           renderViewLess={this.renderViewLess}
                           textStyle={{ marginHorizontal: 11 }}
                       >
                           <Text style={[styles.txtStyle1, { fontSize: 10,width: getWidth(271), marginLeft: 10, color: Colors.textColor }]}>
                               {'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, '}
                           </Text>
                       </ViewMoreText>
                   </View>
               </View>
 
 
 
               <View style={{
                   position: "absolute",
                   height,
                   width,
                   backgroundColor: '#00000'
               }}>
                   <View style={{
                       marginTop: getWidth(145), elevation: 5, flexDirection: 'row', alignSelf: 'center',
                       justifyContent: 'space-between', paddingHorizontal: 10,
                       // paddingVertical:15,
                       backgroundColor: Colors.whiteColor, borderRadius: 10, height: getWidth(100), width: getWidth(300)
                   }}>
                       <View>
                           <Text style={{ fontWeight: '700', fontFamily:Font.Roboto,marginLeft: 26, marginTop: 9, fontFamily: Font.Roboto, fontSize: 16, color: Colors.headingColor }}>
                               {'Ahmad Masjid'}
                           </Text>
 
                           <Text style={{ fontWeight: '400',fontFamily:Font.Roboto, marginTop: 3, width: 180, fontSize: 14, color: Colors.textColor, marginLeft: 26 }}>
                               {'Awal Avenue Corner AI Faith Highway,Bahrain'}
                           </Text>
 
                           <Text style={{ fontSize: 14,fontFamily:Font.Roboto, marginLeft: 26, marginTop: 7, color: Colors.headingColor, fontWeight: '500' }}>
                               {'1.3 Km away'} </Text>
                       </View>
 
 
                       <TouchableOpacity
                           // onPress={props.onBookNow}
                           onPress={() => this.onBookNow()}
                           style={{
                               borderRadius: 11,
                               backgroundColor: Colors.greenColor,
                               elevation: 4,
                               width: 73,
                               height: 22,
                               alignSelf: 'center',
                               justifyContent: 'center',
                               alignItems: 'center'
                           }}
                       >
                           <Text style={{ color: Colors.whiteColor, fontSize: 10,fontFamily:Font.Roboto }} >
                               {"Book Now"}
                           </Text>
                       </TouchableOpacity>
 
                   </View>
               </View>
           </SafeAreaView>
       );
   }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
   container: {
       height,
       width,
       backgroundColor: Colors.backgroundColor,
       justifyContent: 'center'
   },
   mainView: {
       flex: 1,
       backgroundColor: Colors.backGroundColor,
       // justifyContent:'space-between'
   },
   txtStyle1: {
       fontSize: 14,
       fontWeight: '400',
       color: Colors.textColor,
       fontFamily:Font.Roboto
   },
   txtStyle2: {
       fontSize: 14,
       fontWeight: '400',
       color: Colors.headingColor,
       fontFamily:Font.Roboto
   },
 
});
 
const mapStateToProps = (state) => {
   let s = state.indexReducer;
   let stateAkshita = state.indexAkshitaReducer;
 
 
   return {
 
   };
};
 
const mapDsipatchToProps = (dispatch) => {
   return bindActionCreators(
       {
           // setLoginPass: (login_pass) => setLoginPass(login_pass)
       },
 
       dispatch,
   );
};
export default connect(mapStateToProps, mapDsipatchToProps)(MasjidInformationScreen);
 
 
