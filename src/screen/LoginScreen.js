import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, TextInput, Image } from 'react-native';
import Colors from '../common/Colors'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import LoginTopComponent from '../component/LoginTopComponent';
import MyInputText from '../component/MyInputText';
import { getWidth } from '../common/Layout';
import { setLoginMobile, setLoginPass } from '../redux_store/actions/indexAkshitaActions';
import { setCurrentLang, setDropdownIsTrue } from '../redux_store/actions/indexActions';
import Font from '../common/Font';
import CustomeButton from '../component/CustomeButton';
import MyDropdown from '../component/MyDropdown';
import Header from '../component/Header';
import { setShowPass } from '../redux_store/actions/indexActionsJaswant';

class LoginScreen extends Component {

    onClick = () => {
        this.props.navigation.navigate('SignUpScreen')
    }
    onClickHome = () => {
        this.props.navigation.navigate('HomeScreen')
    }

    _renderItem(item, index) {
        return (
            <TouchableOpacity
                onPress={() => {
                    this.props.setDropdownIsTrue(!this.props.is_dropdown)
                    this.props.setCurrentLang(item.item.title, item.item.value)
                }}
                style={{
                    borderBottomWidth: 0.3,
                    borderBottomColor: Colors.textColor,
                    height: 27,
                    justifyContent: 'center',
                    width: getWidth(66)
                }} >

                <Text style={styles.txtStyle}>{item.item.value}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>
                <View style={styles.mainView}>


                    <Header
                        onPress={() => this.props.navigation.goBack()}
                    />

                    <MyDropdown
                        placeholder={this.props.current_lang}
                        // left_icon={require("../assets/location.png")}
                        renderItem={this._renderItem.bind(this)}
                        isDropdownEnabled={this.props.is_dropdown}
                        isVisible={() => this.props.setDropdownIsTrue(!this.props.is_dropdown)}
                        dropdown_list={this.props.language_list}

                        style={{ alignSelf: 'flex-end', width: getWidth(66), marginRight: 30, marginTop: -56, }}
                        ItemStyle={{ height: getWidth(27), marginTop: 34, borderRadius: 3, elevation: 6 }}
                        listStyle={{ height: 125, borderRadius: 2, }}
                        seperatorStyle={{ height: 27, width: 1, backgroundColor: Colors.textColor, marginLeft: 15, }}
                        iconStyle={{ marginLeft: 3 }}
                    />

                    <View style={{
                        marginHorizontal: 36, marginTop: 52,
                        alignItems: this.props.current_lang.includes('R') ? "flex-end" : 'baseline',
                    }}>
                        <Text style={{ fontSize: 24, fontWeight: '500', color: Colors.headingColor, fontFamily: Font.Roboto }}>
                            {this.props.login[this.props.current_lang]}
                        </Text>

                        <Text style={{ marginTop: 12, fontSize: 12, fontWeight: '400', color: Colors.textColor, fontFamily: Font.Roboto }}>
                            {this.props.login_msg[this.props.current_lang]}
                        </Text>
                    </View>


                    <View style={{
                        width: getWidth(300), alignSelf: "center",
                        elevation: 5,
                        backgroundColor: Colors.whiteColor,
                        borderRadius: 14,
                        padding: 5,
                        marginVertical: 20,
                        marginTop: 65
                    }} >
                        <MyInputText
                            placeholder={this.props.mobile_number[this.props.current_lang]}
                            icon={require("../../assets/image/mobile.png")}
                            maxLength={10}
                            keyboardType="phone-pad"
                        />
                        <View style={{ height: 0.5, backgroundColor: Colors.textColor }} />
                        <MyInputText
                            placeholder={this.props.password[this.props.current_lang]}
                            right_text={this.props.show_pass_login ? this.props.hide[this.props.current_lang] : this.props.show[this.props.current_lang]}
                            secureTextEntry={this.props.show_pass_login ? false : true}
                            icon={require("../../assets/image/password.png")}
                            maxLength={15}
                            onPress={() => { this.props.setShowPass(!this.props.show_pass_login) }}
                        />

                    </View>


                    <TouchableOpacity
                        // onPress={() => this.props.setIsForgotPassword(true)}
                        style={{ alignItems: 'flex-end', marginRight: getWidth(36) }}>
                        <Text style={{ fontSize: 12, fontWeight: '400', color: Colors.headingColor, fontFamily: Font.Roboto }}>
                            {this.props.forget_password[this.props.current_lang]}
                        </Text>
                    </TouchableOpacity>


                    <CustomeButton
                        onPress={() => { this.onClickHome() }}
                        title={this.props.login_btn_txt[this.props.current_lang]}
                        height={getWidth(47)}
                        width={getWidth(300)}
                        alignSelf="center"
                        style={{
                            marginTop: 92,
                            backgroundColor: Colors.redColor,
                            elevation: 10,
                            borderRadius: 5,
                            marginVertical: 20,
                            justifyContent: 'flex-end',

                        }}
                        txtStyle={{ color: Colors.whiteColor, fontFamily: Font.Roboto, fontSize: 14, fontWeight: '500', marginRight: this.props.current_lang == "EN" ? 85 : 75 }}
                        iconVisible={true}
                        image={require('../../assets/image/arrow.png')}
                        imageStyle={{
                            marginRight: 10, height: 30, width: 30,
                            backgroundColor: '#AB1126',

                        }}
                    />

                    <TouchableOpacity
                        onPress={() => this.onClick()}
                        style={{ flex: 1, alignSelf: 'center', flexDirection: 'row', marginTop: 20 }} >
                        <Text style={styles.signuptext}>{this.props.do_not_have_acc[this.props.current_lang]} </Text>
                        <Text style={[styles.signuptext, { color: Colors.redColor, fontWeight: '600' }]}>{this.props.register[this.props.current_lang]} </Text>
                    </TouchableOpacity>

                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        height,
        width,
        backgroundColor: Colors.backGroundColor,
    },
    mainView: {
        flex: 1,
        backgroundColor: Colors.whiteText,
    },
    txtStyle: {
        fontSize: 14,
        color: Colors.headingColor,
        fontWeight: '400',
        fontFamily: 'normal',
        marginLeft: 5,
        fontFamily: Font.Roboto
    },

    signuptext: {
        fontSize: 16,
        fontWeight: 'normal',
        fontFamily: Font.SourceSansPro,
        fontWeight: '400',
        color: Colors.textColor
    },

});

const mapStateToProps = (state) => {
    let s = state.indexReducer;
    let stateAkshita = state.indexAkshitaReducer;
    let language = state.indexLangReducer;
    const jas = state.indexReducerJaswant;

    return {
        login_mobile: stateAkshita.login_mobile,
        login_pass: stateAkshita.login_pass,

        current_lang: s.current_lang,
        dropdown_placeholder: s.dropdown_placeholder,
        is_dropdown: s.is_dropdown,
        language_list: s.language_list,

        // Language
        login: language.login,
        login_msg: language.login_msg,
        mobile_number: language.mobile_number,
        password: language.password,
        login_btn_txt: language.login_btn_txt,
        forget_password: language.forget_password,
        create_account: language.create_account,
        show: language.show,
        hide: language.hide,
        register: language.register,
        do_not_have_acc: language.do_not_have_acc,

        // ###################
        show_pass_login: jas.show_pass_login,

    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setLoginMobile: (login_mobile) => setLoginMobile(login_mobile),
            setLoginPass: (login_pass) => setLoginPass(login_pass),
            setCurrentLang: (current_lang, value) => setCurrentLang(current_lang, value),
            setDropdownIsTrue: (isTrue) => setDropdownIsTrue(isTrue),

            setShowPass: (isTrue) => setShowPass(isTrue),

        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(LoginScreen);