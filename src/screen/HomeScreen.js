import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, FlatList, ToastAndroid } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import * as Animatable from 'react-native-animatable';
import Colors from '../common/Colors'
import BottomNav from '../component/BottomNav';
import DrawerOption from '../component/DrawerOption';
import ProfileComponent from '../component/ProfileComponent';
import { getWidth } from '../common/Layout';
import Header from '../component/Header';
import { setBookingCancelStatus, setBookingStatus, setCancelBooking, setIsReady, setSearchInput, setToggleChange } from '../redux_store/actions/indexActionsJaswant';
import SearchInput from '../component/SearchInput';
import HomeHeader from '../component/HomeHeader';
import Masjid_Row from '../row_component/Masjid_Row';
import BookingStatusHeader from '../component/BookingStatusHeader';
import ListHeader from '../component/ListHeader';
import BookingCancelledHeader from '../component/BookingCancelledHeader';
import AlreadyBookesAlert from '../component/AlreadyBookesAlert';
import { setIsAlreadyBooked } from '../redux_store/actions/indexAkshitaActions';
import SearchMasjid from '../component/SearchMasjid';
import { debugLog } from '../common/Constants';

class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ready: false
        }
    }

    handleViewRef = (ref) => (this.view = ref);

    animate = () => {
        if (this.props.is_ready) {
            // this.setState({ ready: false });
            this.props.setIsReady(false)
            this.view.bounceOutLeft(2000);
        } else {
            // this.setState({ ready: true });
            this.props.setIsReady(false)
            this.view.bounceInLeft(2000);
        }
    };

    onClickSuggetion = () => {
        this.props.navigation.navigate("SuggetionScreen")
    }

    onMyBooking = () => {
        this.props.navigation.navigate("MyBookingScreen")
    }
    onLanguage = () => {
        this.props.navigation.navigate("ChangeLanguage")
    }

    onBookNow = (row_data) => {

        this.props.navigation.navigate("BookingConfirmationScreen", { data: row_data })
    }

    onBookInfo = () => {
        this.props.navigation.navigate("MasjidInformationScreen")
    }

    _onBookFull = () => {
        // alert("Comming soon...")
        this.props.setIsAlreadyBooked(!this.props.is_already_booked)
    }
    _onBookingCance = () => {
        let empty = {};
        this.props.setBookingStatus(empty);
    }

    _renderItem = (dataItem) => {
        return (
            <Masjid_Row
                data_row={dataItem.item}
                index={dataItem.index}
                totalBooking={60}
                onPress={() => this.onBookInfo()}
                onBookNow={this.onBookNow}
                onBookFull={() => this._onBookFull()}
                getRowData={this.getRowData}
                // language
                current_lang={this.props.current_lang}
                full_booked={this.props.full_booked[this.props.current_lang]}
                book_now={this.props.book_now[this.props.current_lang]}
            />
        )
    }


    render() {
        // this.props.booking_satus == '[object Object]'?alert('object'): 
        // alert(this.props.booking_satus)
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.mainView}>

                    <HomeHeader
                        toggleText={this.props.near_by[this.props.current_lang]}
                        onPressMenu={() => this.props.setIsReady(!this.props.is_ready)}
                        isMenu={true}

                        is_on={this.props.is_on}
                        onToggleChange={() => {
                            this.props.setToggleChange(!this.props.is_on);
                            ToastAndroid.showWithGravity("Comming soon...", 33, ToastAndroid.TOP)
                        }}

                        onAlert={() => { alert("Comming soon...") }}
                    />

                    {/* <View
                        style={{
                            position: 'absolute',
                            zIndex: 1,
                            elevation: 25,
                            alignSelf: 'flex-end'
                        }}  >
                        <SearchInput
                            onChangeText={(txt) => { this.props.setSearchInput(txt) }}
                            style={{ width: getWidth(300), marginTop: 50, }}
                            inputStyle={{height:40, width: getWidth(150)}}
                        />
                    </View> */}

                    <View style={{
                        flex: 1,
                        backgroundColor: Colors.lightGreenColor,
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        elevation: 15,
                    }} >

                        <SearchMasjid
                            placeholder={this.props.masjid_near_me[this.props.current_lang]}
                            onChangeText={(input) => debugLog(input)}
                            onFocus={() => this.marginT = 50}
                            onBlur={() => this.marginT = 0}
                        />

                        {/* Booking Status */}

                        {
                            Object.keys(this.props.booking_cance_satus).length != 0 ?
                                <BookingCancelledHeader
                                    // Language
                                    placeholder={this.props.masjid_near_me[this.props.current_lang]}
                                    booking_cancel_reason={this.props.booking_cancel_reason[this.props.current_lang]}
                                    book_again={this.props.book_again[this.props.current_lang]}

                                    onCancel={() => this.props.setCancelBooking(!this.props.isBookingCancel)}
                                />
                                :
                                Object.keys(this.props.booking_satus).length != 0 ?
                                    <BookingStatusHeader
                                        onCancelBookning={() => this._onBookingCance()}
                                        // Language

                                        cancel_booking={this.props.cancel_booking[this.props.current_lang]}
                                        navigate_now={this.props.navigate_now[this.props.current_lang]}
                                        placeholder={this.props.masjid_near_me[this.props.current_lang]}
                                        you_have_booked={this.props.you_have_booked[this.props.current_lang]}

                                    /> : null
                        }

                        <View
                            style={{
                                // flex: 2,
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20,
                                marginTop: -50,
                                elevation: 15,
                                backgroundColor: Colors.whiteColor,
                            }}
                        >
                            <ListHeader
                                location={this.props.book_your_location[this.props.current_lang]}
                                available={this.props.available[this.props.current_lang]}
                                booked={this.props.booked[this.props.current_lang]}
                            />
                            <FlatList
                                data={this.props.masjid_list}
                                renderItem={this._renderItem}
                                showsVerticalScrollIndicator={false}
                            />

                        </View>
                    </View>

                    <BottomNav
                        onPressQR={() => this.onMyBooking()}
                        onPressHelp={() => this.onClickSuggetion()}
                        homeIcon={require("../../assets/icon/home.png")}
                        qrImage={require("../../assets/image/qr_h.png")}
                        helpIcon={require("../../assets/image/help.png")}
                        right_label=""
                        left_label=""
                    />


                </View>

                {
                    this.props.is_ready ?
                        <TouchableOpacity
                            onPress={() => {
                                this.props.setIsReady(false)
                                // this.animate
                            }}
                            style={{
                                position: 'absolute',
                                height,
                                width,
                                elevation: 1,
                                backgroundColor: "rgba(0, 0, 0, 0.28)"

                            }}>

                            <Animatable.View
                                style={{
                                    flex: 1,
                                    alignSelf: 'flex-start',
                                    backgroundColor: Colors.lightGray,
                                    width: getWidth(297),
                                    height,
                                    elevation: 5,
                                    backgroundColor: Colors.whiteColor,
                                    position: 'absolute',
                                    // marginTop: -40
                                }}
                                animation="bounceInLeft"
                                ref={this.handleViewRef}>
                                <Header
                                    onPress={() =>
                                        this.props.setIsReady(false)
                                    }
                                />
                                <ProfileComponent
                                    name='Tauseef Ahmed'
                                    status='CPR5874'
                                    profile={require("../../assets/image/elips3.png")}
                                />
                                <ScrollView
                                    showsHorizontalScrollIndicator={false}
                                    contentContainerStyle={{  flex: 1 }}
                                >
                                    <DrawerOption
                                        icon={require('../../assets/image/profile_d.png')}
                                        title={this.props.my_profile[this.props.current_lang]}
                                        onPress={() => { }}
                                    />

                                    <DrawerOption
                                        icon={require('../../assets/image/qr_d.png')}
                                        title={this.props.my_booking[this.props.current_lang]}
                                        onPress={() => { this.onMyBooking() }}
                                    />

                                    <DrawerOption
                                        icon={require('../../assets/image/language_d.png')}
                                        title={this.props.language[this.props.current_lang]}
                                        onPress={() => { this.onLanguage() }}
                                    />

                                    <DrawerOption
                                        icon={require('../../assets/image/invite.png')}
                                        title={this.props.invite_other[this.props.current_lang]}
                                        onPress={() => { }}
                                    />

                                    <DrawerOption
                                        icon={require('../../assets/image/write_d.png')}
                                        title={this.props.write_to_us[this.props.current_lang]}
                                        onPress={() => { }}
                                    />

                                    <DrawerOption
                                        icon={require('../../assets/image/logout.png')}
                                        title={this.props.logout[this.props.current_lang]}
                                        onPress={() => { }}
                                    />
                                    <TouchableOpacity style={{ flex: 1, }} >

                                    </TouchableOpacity>

                                </ScrollView>
                            </Animatable.View>
                        </TouchableOpacity> :
                        null
                }
                {/* <BottomNav
                    onPressQR={() => this.onMyBooking()}
                    onPressHelp={() => this.onClickSuggetion()}
                    homeIcon={require("../../assets/icon/home.png")}
                    qrImage={require("../../assets/image/qr_h.png")}
                    helpIcon={require("../../assets/image/help.png")}
                    right_label=""
                    left_label=""
                /> */}

                {
                    this.props.is_already_booked ?
                        <TouchableOpacity style={{
                            position: 'absolute',
                            width,
                            height,
                            elevation: 5,
                            backgroundColor: 'rgba(0, 0, 0, 0.4)',
                            justifyContent: 'flex-end'
                        }}>

                            <AlreadyBookesAlert
                                onOk={() => {
                                    this.props.setIsAlreadyBooked(!this.props.is_already_booked)
                                }}
                            />

                        </TouchableOpacity>
                        : null
                }

            </SafeAreaView>
        );
    }
}
const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // height, width,
        borderStartColor: 'rgba(0, 0, 0, 0.28)',
    },
    mainView: {
        flex: 1,
        // height, width,
        // justifyContent: 'space-between',
        backgroundColor: Colors.whiteColor
    },
    inputStyle: {
        width: getWidth(300),
        height: 52,
        alignSelf: 'center',
    },
    imgStyle: {
        height: 500,
        width: 500,
        resizeMode: "cover",
    },
    myshadow: {
        shadowColor: '#0008',
        shadowOffset: {
            height: 1,
            width: 0,
        },
        shadowOpacity: 0.5,
        elevation: 11,
    },

});

const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let language = state.indexLangReducer;
    const jas = state.indexReducerJaswant;
    let stateAkshita = state.indexAkshitaReducer;


    return {
        current_lang: common.current_lang,

        login_mobile: stateAkshita.login_mobile,

        is_ready: jas.is_ready,
        is_on: jas.is_on,
        masjid_list: jas.masjid_list,
        isBookingCancel: jas.isBookingCancel,

        // alredy booked
        is_already_booked: stateAkshita.is_already_booked,

        // jaswant 
        booking_satus: jas.booking_satus,
        booking_cance_satus: jas.booking_cance_satus,

        // drawer language
        my_profile: language.my_profile,
        my_booking: language.my_booking,
        language: language.language,
        invite_other: language.invite_other,
        write_to_us: language.write_to_us,
        logout: language.logout,

        // home screen language
        near_by: language.near_by,
        masjid_near_me: language.masjid_near_me,
        booking_cancel_reason: language.booking_cancel_reason,
        book_again: language.book_again,
        book_your_location: language.book_your_location,
        available: language.available,
        booked: language.booked,
        book_now: language.book_now,
        full_booked: language.full_booked,
        cancel_booking: language.cancel_booking,
        navigate_now: language.navigate_now,
        you_have_booked: language.you_have_booked,


    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setLoginMobile: (login_mobile) => setLoginMobile(login_mobile),
            setIsReady: (is_ready) => setIsReady(is_ready),

            setSearchInput: (input) => setSearchInput(input),
            setToggleChange: (is_on) => setToggleChange(is_on),

            // #######################################################

            setIsAlreadyBooked: (is_already_booked) => setIsAlreadyBooked(is_already_booked),
            setCancelBooking: (isTrue) => setCancelBooking(isTrue),
            setBookingStatus: (status) => setBookingStatus(status),
            setBookingCancelStatus: (status) => setBookingCancelStatus(status),
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(HomeScreen);