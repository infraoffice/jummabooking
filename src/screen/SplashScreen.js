import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getContent, setAppLanguage } from '../redux_store/actions/indexActionsJaswant';
import { deleteLanguageContent, getLangText } from '../database/AppDatabase';


class SplashScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('HomeScreen');
    }, 1000);
  }

  render() {

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <TouchableOpacity>
            <Image
              source={require('../../assets/icon/splash.png')}
              style={styles.imgStyle}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height,
    width,
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center'
  },

  textStyle: {
    fontSize: 24,
    fontWeight: 'normal',
    fontFamily: Font.SourceSansPro,
  },
  imgStyle:
  {
    height: 150,
    width: 150
  }

});
const mapStateToProps = (state) => {
  const ss = state.indexReducerJaswant;
  const langStore = state.indexLangReducer;
  const s = state.indexReducer;

  return {
    app_content_list: ss.app_content_list,
    sign_in: langStore.sign_in,
    current_lang: s.current_lang
    // vendor_id: ss.vendor_id,

  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setAppLanguage: () => setAppLanguage(),
      deleteLanguageContent: () => deleteLanguageContent(),
      getContent: () => getContent(),
      // setVenderId: (user_id) => setVenderId(user_id),

    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
