import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import Colors from '../common/Colors'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getWidth } from '../common/Layout';
import { setCurrentLang, setDropdownIsTrue, } from '../redux_store/actions/indexActions';
import CustomeButton from '../component/CustomeButton';
import Header from '../component/Header';


class MobileVerificationScreen extends Component {

    onClick = () => {
        this.props.navigation.navigate('OTPVerificationScreen')
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>
                <View style={styles.mainView}>
                    <Header
                        onPress={() => this.props.navigation.goBack()}
                    />

                    <View style={{ marginTop: 36, }}>
                        <View style={{
                            marginHorizontal: 36, marginTop: 12,
                            alignItems: this.props.current_lang.includes('R') ? "flex-end" : 'baseline'
                        }}>

                            <Text style={{ fontSize: 24, width: 200, fontWeight: '500', color: Colors.headingColor }}>
                                {this.props.verify_mobile[this.props.current_lang]}
                            </Text>

                            <Text style={{ width: 153, marginTop: 3, fontSize: 12, fontWeight: '400', color: Colors.textColor }}>
                                {this.props.two_step_verification[this.props.current_lang]}
                            </Text>

                        </View>
                    </View>

                    <View style={{
                        width: getWidth(300), height: getWidth(56), borderRadius: 14, flexDirection: 'row', alignSelf: 'center',
                        backgroundColor: Colors.whiteColor, elevation: 5, alignItems: 'center', marginTop: 62
                    }}>
                        <Image
                            style={{ height: 25, width: 25, marginLeft: 18 }}
                            source={require('../../assets/image/verify_icon.png')}
                        />
                        <Text style={{ marginHorizontal: 5, color: Colors.headingColor }}>
                            {'+973'}
                        </Text>
                        <Text style={{ borderRightWidth: 1 }} />
                        <TextInput
                            style={{
                                paddingHorizontal: 10,
                                color: Colors.headingColor
                            }}
                            placeholder={this.props.mobile_number[this.props.current_lang]}
                            keyboardType="number-pad"
                            maxLength={10}
                        />
                    </View>




                    <View style={{ justifyContent: 'flex-end', marginTop: 96 }}>
                        <CustomeButton
                            onPress={() => { this.onClick() }}
                            title={this.props.continue[this.props.current_lang]}
                            height={getWidth(47)}
                            width={getWidth(300)}
                            alignSelf="center"
                            style={{

                                backgroundColor: Colors.redColor,
                                elevation: 10,
                                borderRadius: 5,
                                marginBottom: 56,
                                justifyContent: 'flex-end',

                            }}
                            txtStyle={{ color: Colors.whiteColor, fontSize: 14, fontWeight: '500', marginRight: this.props.current_lang == "EN" ? 70 : 80 }}
                            iconVisible={true}
                            image={require('../../assets/image/arrow.png')}
                            imageStyle={{
                                marginRight: 10, height: 30, width: 30,
                                backgroundColor: '#AB1126',

                            }}
                        />
                    </View>

                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        height,
        width,
        backgroundColor: Colors.backgroundColor,
        // justifyContent: 'space-between'
    },
    mainView: {
        flex: 1,
        backgroundColor: Colors.backGroundColor
    },
    txtStyle: {
        fontSize: 14,
        color: Colors.headingColor,
        fontWeight: '400',
        fontFamily: 'normal',
        marginLeft: 29

    },

});

const mapStateToProps = (state) => {
    let s = state.indexReducer;
    let language = state.indexLangReducer;

    return {
        current_lang: s.current_lang,
        dropdown_placeholder: s.dropdown_placeholder,
        is_dropdown: s.is_dropdown,
        language_list: s.language_list,

        // Change language
        verify_mobile: language.verify_mobile,
        two_step_verification: language.two_step_verification,
        mobile_number: language.mobile_number,
        continue: language.continue,

        // ####################3


    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setCurrentLang: (current_lang) => setCurrentLang(current_lang),
            setDropdownIsTrue: (isTrue) => setDropdownIsTrue(isTrue)
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(MobileVerificationScreen);