import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image } from 'react-native';
import Colors from '../common/Colors'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getWidth } from '../common/Layout';
import { setCurrentLang, setDropdownIsTrue, } from '../redux_store/actions/indexActions';
import MyDropdown from '../component/MyDropdown';
import CustomeButton from '../component/CustomeButton';
import Font from '../common/Font';
import Header from '../component/Header';
import { setIsReady } from '../redux_store/actions/indexActionsJaswant';

class ChangeLanguage extends Component {

    onClick = () => {
        this.props.setIsReady(false)
        this.props.navigation.goBack()
    }

    _renderItem(item, index) {
        return (
            <TouchableOpacity
                onPress={() => {
                    this.props.setDropdownIsTrue(!this.props.is_dropdown)
                    this.props.setCurrentLang(item.item.title, item.item.value)
                }}
                style={{
                    borderBottomWidth: 0.3,
                    borderBottomColor: Colors.textColor,
                    height: 50,
                    justifyContent: 'center',
                    width: getWidth(300)
                }} >

                <Text style={styles.txtStyle}>{item.item.title}</Text>
            </TouchableOpacity>

        )
    }



    render() {
        return (

            <SafeAreaView style={styles.container}>
                <View style={styles.mainView}>
                    <Header
                        onPress={() => this.props.navigation.goBack()}
                    />
                    <View style={{ flex: 1, justifyContent: 'space-around', marginTop: 46, alignItems: 'center' }}>

                        <View style={{ alignItems: 'center', marginBottom: -50 }}>
                            <Text style={{
                                fontSize: 24, fontWeight: '500', color: Colors.headingColor, fontFamily: Font.Roboto
                            }}>
                                {this.props.change_lang[this.props.current_lang]}
                            </Text>

                            <Text style={{
                                marginTop: 3, fontSize: 13, fontWeight: '400', color: Colors.textColor, fontFamily: Font.Roboto
                            }}>
                                {this.props.select_lang[this.props.current_lang]}
                            </Text>
                        </View>

                        <MyDropdown
                            placeholder={this.props.dropdown_placeholder}
                            // left_icon={require("../assets/location.png")}
                            renderItem={this._renderItem.bind(this)}
                            isDropdownEnabled={this.props.is_dropdown}
                            isVisible={() => this.props.setDropdownIsTrue(!this.props.is_dropdown)}
                            dropdown_list={this.props.language_list}
                            style={{ alignSelf: 'center', width: getWidth(300) }}
                            ItemStyle={{ height: getWidth(56) }}
                        />

                    </View>

                    <View style={{ flex: 1, justifyContent: 'center', }}>
                        <CustomeButton
                            onPress={() => this.onClick()}
                            title={this.props.continue[this.props.current_lang]}
                            height={getWidth(47)}
                            width={getWidth(300)}
                            alignSelf="center"
                            style={{
                                backgroundColor: Colors.redColor,
                                elevation: 10,
                                borderRadius: 5,
                                justifyContent: 'flex-end',
                            }}

                            txtStyle={{
                                color: Colors.whiteColor, fontSize: 14, fontWeight: '500', marginRight: getWidth(75), fontFamily: Font.Roboto
                            }}
                            iconVisible={true}
                            image={require('../../assets/image/arrow.png')}
                            imageStyle={{
                                marginRight: 10, height: 30, width: 30,
                                backgroundColor: '#AB1126',
                            }}
                        />

                    </View>

                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        height,
        width,
        backgroundColor: Colors.whiteText,
        // justifyContent: 'space-between'
    },
    mainView: {
        flex: 1,
        backgroundColor: Colors.whiteText,
        backgroundColor: Colors.backgroundColor
    },
    txtStyle: {
        fontSize: 14,
        color: Colors.headingColor,
        fontWeight: '400',
        fontFamily: Font.Roboto,
        marginLeft: 29,
        fontFamily: Font.Roboto

    },

});

const mapStateToProps = (state) => {
    let s = state.indexReducer;
    let language = state.indexLangReducer;

    return {
        current_lang: s.current_lang,
        dropdown_placeholder: s.dropdown_placeholder,
        is_dropdown: s.is_dropdown,
        language_list: s.language_list,

        lets_start: language.lets_start,
        select_lang: language.select_lang,
        continue: language.continue,
        change_lang: language.change_lang,
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setCurrentLang: (current_lang, value) => setCurrentLang(current_lang, value),
            setDropdownIsTrue: (isTrue) => setDropdownIsTrue(isTrue),
            setIsReady: (is_ready) => setIsReady(is_ready),
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(ChangeLanguage);