import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, TextInput, ImageBackground } from 'react-native';
import Colors from '../common/Colors'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getWidth } from '../common/Layout';
import { setCurrentLang, setDropdownIsTrue, } from '../redux_store/actions/indexActions';
import MyDropdown from '../component/MyDropdown';
import CustomeButton from '../component/CustomeButton';
import Font from '../common/Font';
import Header from '../component/Header';

class NotificationAlertScreen extends Component {

    onClickAllow = () => {
        this.props.navigation.navigate('LoginScreen')
    }

    onClickSkip = () => {
        this.props.navigation.navigate('LoginScreen')
    }



    render() {
        return (

            <SafeAreaView style={styles.container}>
                <View style={styles.mainView}>
                    <Header
                        onPress={() => this.props.navigation.goBack()}
                    />

                    <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ marginTop: 80 }}>

                            <ImageBackground
                                style={{
                                    height: getWidth(176),
                                    width: getWidth(176),
                                    alignItems: 'center',
                                    justifyContent: 'flex-end',
                                    alignSelf:"center"
                                }}
                                source={require('../../assets/image/ellipse.png')}>
                                <TouchableOpacity>
                                    <Image
                                        style={{ height: getWidth(148), width: getWidth(111), marginBottom: 5 }}
                                        source={require('../../assets/image/notification.png')}
                                    />
                                </TouchableOpacity>
                            </ImageBackground>



                            <Text style={{ fontSize: 24, fontWeight: '500', color: Colors.headingColor, marginTop: 59, textAlign: 'center', fontFamily: Font.Roboto }}>
                                {this.props.notification[this.props.current_lang]}
                            </Text>

                            <Text style={{ width: 210, marginTop: 5, fontSize: 12, fontWeight: '400', color: Colors.textColor, textAlign: 'center',fontFamily:Font.Roboto }}>
                                {this.props.notification_msg[this.props.current_lang]}
                            </Text>
                        </View>

                        <View style={{}}>
                            <CustomeButton
                                onPress={() => { this.onClickAllow() }}
                                title={this.props.allow[this.props.current_lang]}
                                height={getWidth(47)}
                                width={getWidth(141)}
                                alignSelf="center"
                                style={{
                                    marginTop: 72,
                                    backgroundColor: Colors.redColor,
                                    elevation: 10,
                                    borderRadius: 10,
                                    justifyContent: 'flex-end',

                                }}
                                txtStyle={{ color: Colors.whiteColor, fontSize: 14, marginRight: 10,fontFamily:Font.Roboto }}
                                iconVisible={true}
                                image={require('../../assets/image/arrow.png')}
                                imageStyle={{
                                    marginRight: 10, height: 26, width: 26,
                                    backgroundColor: '#AB1126'

                                }}
                            />

                            <TouchableOpacity
                                onPress={() => { this.onClickSkip() }}
                            >
                                <Text style={{ textAlign: 'center', marginTop: 39, fontSize: 14, color: Colors.textColor,fontFamily:Font.Roboto }}>
                                    {this.props.skip[this.props.current_lang]}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        height,
        width,
        backgroundColor: Colors.whiteText,
        // justifyContent: 'space-between'
    },
    mainView: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },
    txtStyle: {
        fontSize: 14,
        color: Colors.headingColor,
        fontWeight: '400',
        fontFamily: 'normal',
        marginLeft: 29

    },

});

const mapStateToProps = (state) => {
    let s = state.indexReducer;
    let language = state.indexLangReducer;

    return {
        current_lang: s.current_lang,
        dropdown_placeholder: s.dropdown_placeholder,
        is_dropdown: s.is_dropdown,
        language_list: s.language_list,

        // Language
        notification: language.notification,
        notification_msg: language.notification_msg,
        allow: language.allow,
        skip: language.skip,

    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setCurrentLang: (current_lang) => setCurrentLang(current_lang),
            setDropdownIsTrue: (isTrue) => setDropdownIsTrue(isTrue)
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(NotificationAlertScreen);