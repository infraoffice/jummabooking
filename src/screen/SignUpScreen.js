import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CustomeButton from '../component/CustomeButton';
import Colors from '../common/Colors'
import { getWidth } from '../common/Layout';
import MyInputText from '../component/MyInputText';
import { setSignupMobile, setSignupName, setSignupPass, setSignupRePass } from '../redux_store/actions/indexAkshitaActions';
import { setAppLanguage, setShowCPR, setShowPassRegist, setShowRePass } from '../redux_store/actions/indexActionsJaswant';
import Header from '../component/Header';

class SignUpScreen extends Component {

    onLogin = () => {
        this.props.navigation.navigate('LoginScreen')
    }

    onRegistProfile = () => {
        this.props.navigation.navigate('ProfileRegistrationScreen')
    }

    render() {

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>
                    <Header
                        onPress={() => this.props.navigation.goBack()}
                    />
                    <ScrollView>
                        <View
                            style={{
                                alignItems: this.props.current_lang.includes('R') ? "flex-end" : 'baseline',
                                marginHorizontal: 36,
                            }}
                        >
                            <Text style={{ marginTop: 40, marginLeft: 30, fontSize: 24, fontWeight: '500', fontFamily: Font.Roboto }}>
                                {this.props.register[this.props.current_lang]}
                            </Text>

                            <Text style={{ marginTop: 20, marginLeft: 30, color: '#AAB1BB', fontSize: 14, fontWeight: '400', fontFamily: Font.Roboto }}>
                                {this.props.registration_msg[this.props.current_lang]}
                            </Text>

                        </View>



                        <View style={{
                            width: getWidth(300),
                            alignSelf: "center",
                            elevation: 5,
                            backgroundColor: Colors.whiteColor,
                            borderRadius: 14,
                            padding: 5,
                            marginVertical: 20,
                            marginTop: 53
                        }} >
                            <MyInputText
                                placeholder={this.props.name[this.props.current_lang]}
                                icon={require("../../assets/image/profile_d.png")}
                                maxLength={10}
                                keyboardType="phone-pad"
                                onChangeText={(text) => {
                                    this.props.setSignupName(text);
                                }}
                                iconStyle={{ width: 18, height: 20 }}
                            />
                            <View style={{ height: 0.5, backgroundColor: Colors.textColor }} />


                            <MyInputText
                                placeholder={this.props.cpr_number[this.props.current_lang]}
                                right_text={this.props.show_cpr_num ? this.props.hide[this.props.current_lang] : this.props.show[this.props.current_lang]}
                                secureTextEntry={this.props.show_cpr_num ? false : true}
                                icon={require("../../assets/image/mobile.png")}
                                maxLength={15}
                                onChangeText={(text) => {
                                    this.props.setSignupCprNo(text);
                                }}
                                onPress={() => { this.props.setShowCPR(!this.props.show_cpr_num) }}
                            />
                            <View style={{ height: 0.5, backgroundColor: Colors.textColor }} />


                            <MyInputText
                                placeholder={this.props.mobile_placeholder[this.props.current_lang]}
                                icon={require("../../assets/image/mobile.png")}
                                maxLength={10}
                                keyboardType="phone-pad"
                                onChangeText={(text) => {
                                    this.props.setSignupMobile(text);
                                }}
                            />
                            <View style={{ height: 0.5, backgroundColor: Colors.textColor }} />


                            <MyInputText
                                placeholder={this.props.password[this.props.current_lang]}
                                right_text={this.props.show_pass_rag ? this.props.hide[this.props.current_lang] : this.props.show[this.props.current_lang]}
                                secureTextEntry={this.props.show_pass_rag ? false : true}
                                icon={require("../../assets/image/password.png")}
                                maxLength={15}
                                onChangeText={(text) => {
                                    this.props.setSignupPass(text);
                                }}
                                onPress={() => { this.props.setShowPassRegist(!this.props.show_pass_rag) }}
                            />
                            <View style={{ height: 0.5, backgroundColor: Colors.textColor }} />

                            <MyInputText
                                placeholder={this.props.re_enter_assword[this.props.current_lang]}
                                right_text={this.props.show_re_enter_pass ? this.props.hide[this.props.current_lang] : this.props.show[this.props.current_lang]}
                                secureTextEntry={this.props.show_re_enter_pass ? false : true}
                                icon={require("../../assets/image/password.png")}
                                maxLength={15}
                                onChangeText={(text) => {
                                    this.props.setSignupRePass(text);
                                }}
                                onPress={() => { this.props.setShowRePass(!this.props.show_re_enter_pass) }}
                            />
                        </View>

                        <CustomeButton
                            onPress={() => { this.onRegistProfile() }}
                            title={this.props.continue[this.props.current_lang]}
                            height={getWidth(47)}
                            width={getWidth(300)}
                            alignSelf="center"
                            style={{
                                marginTop: 10,
                                backgroundColor: Colors.redColor,
                                elevation: 10,
                                borderRadius: 5,
                                // marginBottom: 56,
                                justifyContent: 'flex-end',
                            }}

                            txtStyle={{ color: Colors.whiteColor, fontSize: 18, fontWeight: '500', marginRight: this.props.current_lang == "EN" ? 60 : 75 }}
                            iconVisible={true}
                            image={require('../../assets/image/arrow.png')}
                            imageStyle={{
                                marginRight: 10, height: 30, width: 30,
                                backgroundColor: '#AB1126',

                            }}
                        />


                        <TouchableOpacity
                            onPress={() => this.onLogin()}
                            style={{ flex: 1, alignSelf: 'center', flexDirection: 'row', marginTop: 20 }} >
                            <Text style={styles.signuptext}>{this.props.already_have_acc[this.props.current_lang]} </Text>
                            <Text style={[styles.signuptext, { color: Colors.redColor, fontWeight: '600' }]}>{this.props.login[this.props.current_lang]} </Text>
                        </TouchableOpacity>


                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}
// const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F5F9',
    },

    signuptext: {
        fontSize: 16,
        fontWeight: 'normal',
        fontFamily: Font.SourceSansPro,
        fontWeight: '400',
        color: Colors.textColor
    },
    imgStyle:
    {
        height: 150,
        width: 150
    }

});
const mapStateToProps = (state) => {
    const jas = state.indexReducerJaswant;
    const language = state.indexLangReducer;
    const s = state.indexReducer;
    let stateAkshita = state.indexAkshitaReducer;


    return {
        current_lang: s.current_lang,
        signup_name: stateAkshita.signup_name,
        signup_cpr_no: stateAkshita.signup_cpr_no,
        signup_mobile: stateAkshita.signup_mobile,
        signup_pass: stateAkshita.signup_pass,
        signup_re_pass: stateAkshita.signup_re_pass,

        // Language
        register: language.register,
        registration_msg: language.registration_msg,
        login: language.login,
        name: language.name,
        cpr_number: language.cpr_number,
        mobile_placeholder: language.mobile_placeholder,
        password: language.password,
        re_enter_assword: language.re_enter_assword,
        login_btn_txt: language.login_btn_txt,
        forget_password: language.forget_password,
        already_have_acc: language.already_have_acc,
        show: language.show,
        hide: language.hide,
        continue: language.continue,
        do_not_have_acc: language.do_not_have_acc,

        // ########################
        show_pass_rag: jas.show_pass_rag,
        show_cpr_num: jas.show_cpr_num,
        show_re_enter_pass: jas.show_re_enter_pass,

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setAppLanguage: () => setAppLanguage(),
            setSignupName: (signup_name) => setSignupName(signup_name),
            setSignupCprNo: (signup_cpr_no) => setSignupCprNo(signup_cpr_no),
            setSignupMobile: (signup_mobile) => setSignupMobile(signup_mobile),
            setSignupPass: (signup_pass) => setSignupPass(signup_pass),
            setSignupRePass: (signup_re_pass) => setSignupRePass(signup_re_pass),

            // from Jaswant action
            setShowPassRegist: (isTrue) => setShowPassRegist(isTrue),
            setShowRePass: (isTrue) => setShowRePass(isTrue),
            setShowCPR: (isTrue) => setShowCPR(isTrue),

        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);


