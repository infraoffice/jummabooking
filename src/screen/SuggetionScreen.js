import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, StatusBar } from 'react-native';
import Header from '../component/Header';
import Colors from '../common/Colors'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getWidth } from '../common/Layout';
import { setLoginMobile, setLoginPass } from '../redux_store/actions/indexAkshitaActions';

class SuggetionScreen extends Component {

    onClick = () => {
        this.props.navigation.navigate('HomeScreen')
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>
                <StatusBar />
                <Header
                    onPress={() => this.props.navigation.goBack()}
                    title='Help'
                    style={{
                        backgroundColor: Colors.whiteColor,
                        height: 50,
                        padding: 5,
                        flexDirection: "row",
                        justifyContent: "flex-start",
                        alignItems: "center"
                    }}
                />
                <View style={styles.mainView}>

                    <Image
                        source={require("../../assets/icon/suggetion.png")}
                        style={{ resizeMode: 'contain', flex: 1, }}
                    />

                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.whiteText,
    },
    mainView: {
        flex: 1,
        backgroundColor: Colors.whiteText,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },

    txtStyle: {
        fontSize: 28,
        color: 'blue',
        fontFamily: 'serif'
    },

});

const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let language = state.indexLangReducer;
    let stateAkshita = state.indexAkshitaReducer;


    return {
        current_lang: common.current_lang,

    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setLoginMobile: (login_mobile) => setLoginMobile(login_mobile),
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(SuggetionScreen);