import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import Colors from '../common/Colors'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getWidth } from '../common/Layout';
import { setCurrentLang, setDropdownIsTrue, } from '../redux_store/actions/indexActions';
import MyDropdown from '../component/MyDropdown';
import CustomeButton from '../component/CustomeButton';
import Font from '../common/Font';
import OTPTextInput from '../component/OTPTextInput';
import Header from '../component/Header';
import { setOTP, } from '../redux_store/actions/indexAkshitaActions';
class OTPVerificationScreen extends Component {
    constructor(props) {
        super(props);

    }

    onClick = () => {
        this.props.navigation.navigate('NotificationAlertScreen')
    }

    onResend = () => {
        this.props.navigation.navigate('MobileVerificationScreen')
    }


    render() {
        return (

            <SafeAreaView style={styles.container}>
                <View style={styles.mainView}>

                    <Header
                        onPress={() => this.props.navigation.goBack()}
                    />


                    <View style={{
                        marginTop: 36, marginHorizontal: 36,
                        alignItems: this.props.current_lang.includes('R') ? "flex-end" : 'baseline'
                    }}>
                        <Text style={{ fontSize: 24, width: 153, fontWeight: '500', color: Colors.headingColor }}>
                            {this.props.verification[this.props.current_lang]}
                        </Text>

                        <Text style={{ width: 153, marginTop: 3, fontSize: 12, fontWeight: '400', color: Colors.textColor }}>
                            {this.props.verification_msg[this.props.current_lang]}
                        </Text>
                    </View>


                    <OTPTextInput
                        value={this.props.otp}
                        onChange={(otp) => {
                            this.props.setOTP(otp)
                        }}
                    />


                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 24 }}>
                        <Text style={{ fontWeight: '600', color: Colors.textColor, fontSize: 14, fontStyle: 'normal' }}>
                            {this.props.did_not_receive[this.props.current_lang]} </Text>
                        <TouchableOpacity onPress={() => { this.onResend() }} >
                            <Text style={{ fontWeight: 'bold', color: Colors.redColor, fontSize: 14, fontStyle: 'normal' }}>
                                {this.props.resend[this.props.current_lang]}</Text>
                        </TouchableOpacity>
                    </View>

                    <CustomeButton
                        onPress={() => { this.onClick() }}
                        title={this.props.continue[this.props.current_lang]}
                        height={getWidth(47)}
                        width={getWidth(300)}
                        alignSelf="center"
                        style={{
                            backgroundColor: Colors.redColor,
                            elevation: 10,
                            borderRadius: 5,
                            marginTop: 32,
                            justifyContent: 'flex-end',

                        }}
                        txtStyle={{ color: Colors.whiteColor, fontSize: 14, fontWeight: '500', marginRight: this.props.current_lang == "EN" ? 70 : 85 }}
                        iconVisible={true}
                        image={require('../../assets/image/arrow.png')}
                        imageStyle={{
                            marginRight: 10, height: 30, width: 30,
                            backgroundColor: '#AB1126',

                        }}
                    />

                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        height,
        width,
        backgroundColor: Colors.whiteText,
        // justifyContent: 'space-between'
    },
    mainView: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    txtStyle: {
        fontSize: 14,
        color: Colors.headingColor,
        fontWeight: '400',
        fontFamily: 'normal',
        marginLeft: 5

    },

});

const mapStateToProps = (state) => {
    let s = state.indexReducer;
    let language = state.indexLangReducer;
    let stateAkshita = state.indexAkshitaReducer;

    return {
        current_lang: s.current_lang,
        is_dropdown: s.is_dropdown,

        // Language
        verification: language.verification,
        verification_msg: language.verification_msg,
        did_not_receive: language.did_not_receive,
        resend: language.resend,
        continue: language.continue,


        // #######################################
        otp: stateAkshita.otp,



    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setDropdownIsTrue: (isTrue) => setDropdownIsTrue(isTrue),

            setOTP: (otp_first) => setOTP(otp_first),
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(OTPVerificationScreen);