import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Text, TouchableOpacity, Image, } from 'react-native';
import Colors from '../common/Colors'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { setIsChecked, setLoginMobile, setLoginPass } from '../redux_store/actions/indexAkshitaActions';
import { setCurrentLang, setDropdownIsTrue } from '../redux_store/actions/indexActions';

import Header from '../component/Header';
import { setBookingStatus, setShowPass } from '../redux_store/actions/indexActionsJaswant';
import { getWidth } from '../common/Layout';
import Font from '../common/Font';
import CustomeButton from '../component/CustomeButton';
import ConfBooking from '../component/ConfBooking';

class BookingConfirmationScreen extends Component {

    onBookingConf = () => {
        this.props.setBookingStatus(this.props.route.params.data)
        this.props.navigation.navigate("HomeScreen")
    }

    onBookInfo = () => {
        this.props.navigation.navigate("MasjidInformationScreen")
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>
                <View style={styles.mainView}>

                    <Header
                        onPress={() => this.props.navigation.goBack()}
                    />


                    <ConfBooking
                        get_details={this.props.get_details[this.props.current_lang]}
                        onPress={() => this.onBookInfo()}
                    />


                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 88 }}>


                        <TouchableOpacity style={{}}>
                            <Image
                                style={{ height: getWidth(104), width: getWidth(173) }}
                                source={require('../../assets/image/mask.png')}
                            />
                            <Image
                                style={{ height: getWidth(5), width: getWidth(62), alignSelf: 'center' }}
                                source={require('../../assets/image/line.png')}
                            />
                        </TouchableOpacity>

                    </View>




                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 40 }}>

                        <TouchableOpacity
                            onPress={() => this.props.setIsChecked(!this.props.is_checked)}
                            style={{
                            }}>
                            <Image
                                style={{ height: this.props.is_checked ? 20 : 20, width: this.props.is_checked ? 20 : 20, marginRight: 11 }}
                                source={this.props.is_checked ? require('../../assets/image/checked.png') :
                                    require('../../assets/image/unchecked.png')
                                }
                            />
                        </TouchableOpacity>

                        <Text style={{ fontWeight: '400', width: 291, fontSize: 14, color: Colors.textColor, marginLeft: 11, fontFamily: Font.Roboto }}>
                            {this.props.confirmation_msg[this.props.current_lang]}
                        </Text>

                    </View>

                    <CustomeButton
                        onPress={() => { this.onBookingConf() }}
                        title={this.props.confirm_booking[this.props.current_lang]}
                        height={getWidth(47)}
                        width={getWidth(300)}
                        alignSelf="center"
                        style={{
                            marginTop: 29,
                            backgroundColor: Colors.greenColor,
                            elevation: 10,
                            borderRadius: 5,
                            justifyContent: 'flex-end',
                        }}
                        txtStyle={{ color: Colors.whiteColor, fontSize: 18, fontWeight: '500', marginRight: this.props.current_lang == "EN" ? 20 : 60, fontFamily: Font.Roboto }}
                        iconVisible={true}
                        image={require('../../assets/image/arrow.png')}
                        imageStyle={{
                            marginRight: 10, height: 30, width: 30,
                            backgroundColor: '#05a105',

                        }}
                    />

                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        height,
        width,
        backgroundColor: Colors.backGroundColor,
    },
    mainView: {
        flex: 1,
        backgroundColor: Colors.whiteText,
    },

});

const mapStateToProps = (state) => {
    let s = state.indexReducer;
    let stateAkshita = state.indexAkshitaReducer;
    let language = state.indexLangReducer;
    const jas = state.indexReducerJaswant;

    return {
        login_mobile: stateAkshita.login_mobile,
        login_pass: stateAkshita.login_pass,

        current_lang: s.current_lang,
        is_checked: stateAkshita.is_checked,

        // Language
        booking_confirmation: language.booking_confirmation,
        get_details: language.get_details,
        confirmation_msg: language.confirmation_msg,
        confirm_booking: language.confirm_booking
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setIsChecked: (is_checked) => setIsChecked(is_checked),
            setBookingStatus: (status) => setBookingStatus(status),
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(BookingConfirmationScreen);