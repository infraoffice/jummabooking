import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';
import Header from '../component/Header';
import Colors from '../common/Colors'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { getWidth } from '../common/Layout';
import BottomNav from '../component/BottomNav';
import Font from '../common/Font';
import { setBookingStatus } from '../redux_store/actions/indexActionsJaswant';

class MyBookingScreen extends Component {




    _onBookingCance = () => {
        let empty = {};
        this.props.setBookingStatus(empty);
        this.props.navigation.navigate("HomeScreen");
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>
                <View style={styles.mainView}>

                    <Header
                        onPress={() => this.props.navigation.goBack()}
                        title={this.props.my_booking[this.props.current_lang]}
                    />

                    <View style={{ marginTop: 52, alignItems: 'center' }}>
                        <Text style={{ fontWeight: '700', fontFamily: Font.Roboto, fontSize: 16, color: Colors.headingColor }}>
                            {'Ahmad Masjid'}
                        </Text>

                        <Text style={{ fontWeight: '400', fontFamily: Font.Roboto, fontSize: 14, color: Colors.textColor }}>
                            {'for Juma’a Salah'}

                        </Text>
                    </View>


                    <View style={{ alignItems: 'center', justifyContent: 'space-between' }}>
                        <Text style={{ marginTop: 15, fontWeight: '400', fontSize: 14, color: Colors.textColor, fontFamily: Font.Roboto }}>
                            {'Dec 12, 2020 at 11:30 AM'}
                        </Text>

                        <TouchableOpacity style={{
                            width: getWidth(204), height: getWidth(204),
                            backgroundColor: Colors.whiteColor, alignItems: 'center',
                            justifyContent: 'center', elevation: 1, marginTop: 15
                        }}>
                            <Image
                                style={{ height: getWidth(170), width: getWidth(170) }}
                                source={require('../../assets/image/qrcode.png')}
                            />
                        </TouchableOpacity>
                        <Text style={{ marginTop: 6, fontFamily: Font.Roboto, fontWeight: '400', fontSize: 10, color: Colors.textColor }}>
                            {this.props.qr_scan_suggestion[this.props.current_lang]}
                        </Text>
                    </View>


                    <TouchableOpacity
                        onPress={() => this.onGetDirection()}
                        style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 57 }}>
                        <Text style={{ fontWeight: '400', fontFamily: Font.Roboto, fontSize: 16, color: Colors.greenColor }}>
                            {this.props.get_direction[this.props.current_lang]} </Text>
                        <Image
                            style={{ height: getWidth(15), width: getWidth(20), marginLeft: 10 }}
                            source={require('../../assets/image/vector.png')}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this._onBookingCance()}
                        style={{
                            borderRadius: 23.5,
                            height: getWidth(38),
                            width: getWidth(142),
                            borderWidth: 1,
                            borderColor: Colors.redColor,
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginTop: 51,
                            alignSelf: 'center'
                        }}>
                        <Text style={{ color: Colors.redColor, fontWeight: '500', fontSize: 14, fontFamily: Font.Roboto }}>
                            {this.props.cancel_booking[this.props.current_lang]}
                        </Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        height,
        width,
        backgroundColor: Colors.whiteText,
        // justifyContent: 'space-between'
    },
    mainView: {
        flex: 1,
        backgroundColor: Colors.backGroundColor,
        // justifyContent:'space-between'
    },
    txtStyle: {
        fontSize: 28,
        color: 'blue',
        fontFamily: 'serif'
    },

});

const mapStateToProps = (state) => {
    let s = state.indexReducer;
    let stateAkshita = state.indexAkshitaReducer;
    const language = state.indexLangReducer;

    return {
        current_lang: s.current_lang,

        // Language
        my_booking: language.my_booking,
        qr_scan_suggestion: language.qr_scan_suggestion,
        get_direction: language.get_direction,
        cancel_booking: language.cancel_booking

    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setLoginPass: (login_pass) => setLoginPass(login_pass)
            setBookingStatus: (status) => setBookingStatus(status),
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(MyBookingScreen);