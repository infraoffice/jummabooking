import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    Image,
    TouchableOpacity,
    ScrollView,
    Alert
} from 'react-native';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CustomeButton from '../component/CustomeButton';
import Colors from '../common/Colors'
import { getWidth } from '../common/Layout';
import MyInputText from '../component/MyInputText';
import { setProfilePic, setSignupAddressArea, setSignupAddressBlock, setSignupAddressBuilding, setSignupAddressRoad } from '../redux_store/actions/indexAkshitaActions';
import { setAppLanguage } from '../redux_store/actions/indexActionsJaswant';
import Header from '../component/Header';
import ImagePicker from 'react-native-image-crop-picker';


class ProfileRegistrationScreen extends Component {

    onClickHome = () => {
        this.props.navigation.navigate('HomeScreen')
    }

    openPickerGallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            isCamera: true,
        }).then((image) => {
            this.props.setProfilePic(image);
        });
    };

    openPickerCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 300,
            cropping: true,
        }).then((image) => {
            this.props.setProfilePic(image);
        });
    };

    showAlert() {
        Alert.alert(
            'Upload logo',
            'Please choose Camera or Gallery',
            [
                {
                    text: 'Camera',
                    onPress: () => {
                        this.openPickerCamera();
                    },
                },
                {
                    text: 'Gallery',
                    onPress: () => {
                        this.openPickerGallery();
                    },
                },
            ],
            {
                cancelable: false,
            },
        );
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>
                    <Header
                        onPress={() => this.props.navigation.goBack()}
                    />
                    <Text style={{ marginTop: 36, marginLeft: 30, fontSize: 24, fontWeight: '500', fontFamily: Font.Roboto }}>
                        {this.props.profile[this.props.current_lang]}
                    </Text>


                    <View style={{
                        width: getWidth(300),
                        height:340,
                        alignSelf: "center",
                        elevation: 5,
                        backgroundColor: Colors.whiteColor,
                        borderRadius: 14,
                        // padding: 5,
                        marginVertical: 26,
                        marginTop: 45
                    }} >

                        <View style={{ height: 100, flexDirection: 'row', alignItems: 'center',borderTopRightRadius:14,borderTopLeftRadius:14, backgroundColor: Colors.whiteColor }}>
                        <TouchableOpacity onPress={() => this.showAlert()}>
                                <Image
                                    style={{ height: 62, width: 62, marginLeft: 24,resizeMode:'contain',borderRadius:30 }}
                                    source={this.props.set_profile != '' ?
                                    {uri:this.props.set_profile.path}:
                                    require('../../assets/image/elips3.png')}
                                />
                            </TouchableOpacity>
                            <View>
                                <Text style={[styles.txtStyle, { color: Colors.headingColor, marginLeft: 14 }]} >{'Tauseef Ahmed'} </Text>
                                <Text style={[styles.txtStyle, { fontSize: 14, marginLeft: 14 }]} >{'CPR5874'} </Text>
                            </View>

                        </View>

                        <ScrollView
                            showsVerticalScrollIndicator={false}
                        >

                            <View style={{ height: 40, flexDirection: 'row', alignItems: 'center', backgroundColor: Colors.backgroundColor }}>
                                <TouchableOpacity>
                                    <Image
                                        style={{ height: 20, width: 15, marginLeft: 10, }}
                                        source={require('../../assets/image/address.png')}
                                    />
                                </TouchableOpacity>
                                <Text style={[styles.txtStyle, { fontSize: 16 ,flex:1,marginRight: 30,marginLeft:10}]} >
                                    {this.props.address[this.props.current_lang]}
                                </Text>
                            </View>


                            <View style={{ height: 0.5, backgroundColor: Colors.textColor }} />
                            <MyInputText
                                placeholder={this.props.area[this.props.current_lang]}
                                maxLength={15}
                                onChangeText={(text) => {
                                    this.props.setSignupAddressArea(text);
                                }}
                            />
                            <View style={{ height: 0.5, backgroundColor: Colors.textColor }} />
                            <MyInputText
                                placeholder= {this.props.block[this.props.current_lang]}
                                maxLength={15}
                                keyboardType="default"
                                onChangeText={(text) => {
                                    this.props.setSignupAddressBlock(text);
                                }}
                            />

                            <View style={{ height: 0.5, backgroundColor: Colors.textColor }} />
                            <MyInputText
                                placeholder= {this.props.road[this.props.current_lang]}
                                maxLength={15}
                                keyboardType="default"
                                onChangeText={(text) => {
                                    this.props.setSignupAddressRoad(text);
                                }}
                            />
                            <View style={{ height: 0.5, backgroundColor: Colors.textColor }} />
                            <MyInputText
                                placeholder= {this.props.building_villa[this.props.current_lang]}
                                maxLength={15}
                                keyboardType="default"
                                onChangeText={(text) => {
                                    this.props.setSignupAddressBuilding(text);
                                }}
                            />
                        </ScrollView>
                    </View>


                    <CustomeButton
                        onPress={() => { this.onClickHome() }}
                        title={this.props.register[this.props.current_lang]}
                        height={getWidth(47)}
                        width={getWidth(300)}
                        alignSelf="center"
                        style={{
                            // marginTop: 26,
                            backgroundColor: Colors.redColor,
                            elevation: 10,
                            borderRadius: 5,
                            marginBottom: 96,
                            justifyContent: 'flex-end',

                        }}
                        txtStyle={{ color: Colors.whiteColor,fontFamily:Font.Roboto, fontSize: 18, fontWeight: '500',marginRight: this.props.current_lang=="EN"?60:75 }}
                        iconVisible={true}
                        image={require('../../assets/image/arrow.png')}
                        imageStyle={{
                            marginRight: 10, height: 30, width: 30,
                            backgroundColor: '#AB1126',

                        }}
                    />


                </View>
            </SafeAreaView>
        );
    }
}
// const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F5F9',
    },

    txtStyle: {
        fontSize: 18,
        fontWeight: '400',
        fontFamily: Font.Roboto,
        color: Colors.textColor,
    },
    imgStyle:
    {
        height: 150,
        width: 150
    }

});
const mapStateToProps = (state) => {
    const ss = state.indexReducerJaswant;
    const language = state.indexLangReducer;
    const s = state.indexReducer;
    let stateAkshita = state.indexAkshitaReducer;


    return {
        current_lang: s.current_lang,
        signup_address_area: stateAkshita.signup_address_area,
        signup_address_block: stateAkshita.signup_address_block,
        signup_address_road: stateAkshita.signup_address_road,
        signup_address_building: stateAkshita.signup_address_building,
        set_profile: stateAkshita.set_profile,


        // Language
        profile: language.profile,
        address: language.address,
        area: language.area,
        block: language.block,
        road: language.road,
        building_villa: language.building_villa,
        register: language.register
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setAppLanguage: () => setAppLanguage(),
            setSignupAddressArea: (signup_address_area) => setSignupAddressArea(signup_address_area),
            setSignupAddressBlock: (signup_address_block) => setSignupAddressBlock(signup_address_block),
            setSignupAddressRoad: (signup_address_road) => setSignupAddressRoad(signup_address_road),
            setSignupAddressBuilding: (signup_address_building) => setSignupAddressBuilding(signup_address_building),
            setProfilePic: (update_profile) => setProfilePic(update_profile)

        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(ProfileRegistrationScreen);